import React from 'react';


import Home from '../example'
import Timesheet from '../Timesheet'
import {Route,Switch} from 'react-router-dom'
import Header from '../Layout/Header'
import Sidebar from '../Layout/Sidebar'
 import Loker from '../Sumberloker'
 import Leave from '../Leave'
 import User from '../User'
import Category from '../Category'





export default class content extends React.Component{
  render(){
    return(
      <div className="wrapper">
          <Header/>
          <Sidebar/>
              <div className="content-wrapper">
            <section className ="content">
              <Switch>
                {/* <Route exact path='/Home'component={Home}/>
                 <Route exact path='/Leave' component={Leave} />
                <Route exact path='/Timesheet' component={Timesheet} />
                <Route exact path='/Loker' component={Loker} />
                <Route exact path='/User' component={User} />  */}
                <Route exact path='/Category' component={Category} />

              </Switch>
            </section>

          </div>
      
      </div>
    )
  }
}
