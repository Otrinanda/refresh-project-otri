import axios from 'axios';
import {config} from '../config/config';
 const SelectService ={
    getBulan :()=>{
        const result = axios.get(config.apiUrl+'/getbulan')
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    }
    
}
export default SelectService;