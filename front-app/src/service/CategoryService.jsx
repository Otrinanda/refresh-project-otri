import axios from "axios";
import {config} from '../config/config';

const CategoryService={
    getAll:()=>{
        const result=axios.get(config.apiUrl+'/getall')
        .then(respons=>{
            return{
                success:respons.data.success,
                result:respons.data.result 
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    },
    getrespondenbyid: (id) => {
        const result = axios.get(config.apiUrl + '/getrespondenbyid/'+id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/postresponden', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    deleteresponden: (item) => {
        const result = axios.put(config.apiUrl + '/deleteresponden/'+item.Id,item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateResponden: (item) => {
        const result = axios.put(config.apiUrl + '/updateresponden/'+item.Id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    getStatus :()=>{
        const result = axios.get(config.apiUrl+'/getstatus')
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    },
};



export default CategoryService;