import React from 'react';
import Content from './Layout/Content';
import './App.css';

import { BrowserRouter,  Route, Switch } from 'react-router-dom'

import Login from './Authentication/Login'

import Category from './Category/index'

export default class App extends React.Component {
  render() {
    return (
     
        <BrowserRouter>
       
          <Switch>
            <Route exact path='/' component={Category}/>
            <Content />
          </Switch>
          
        </BrowserRouter>      
     
    )
  }
}
