import React from "react";
import Select from "react-select";
import "bootstrap/dist/css/bootstrap.min.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

class FormResponden extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedOption: {}
    };
  }

  render() {
    const {
      handleTampil,
      changeHandler,
      RespondenModel,
      onSave,
      cancelHandler,
      selectedHandler,
      mode,
      openmodal,
      StatusOption
    } = this.props;

    //   let pilihanStatus = StatusOption.map(function (item) {
    //     return { value: item.StatusId, label: item.StatusNama }
    // })

    return (
      <div>
        <Button variant="primary" onClick={handleTampil}>
          {" "}
          Create
        </Button>
        <br></br>

        <Modal show={openmodal} style={{ opacity: 1 }}>
          <Modal.Header style={{ backgroundColor: "lightblue" }}>
            <Modal.Title> {mode} </Modal.Title>
          </Modal.Header>
          <Modal.Body>
          {JSON.stringify(StatusOption)}
            <div className="form-inside-input">
              <br />
              <label className="form-label"> Nama Lengkap</label>
              <input
                type="text"
                id="Nama"
                className="form-control"
                value={RespondenModel.Nama}
               onChange={changeHandler("Nama")}
              />
              <br />
              <label className="form-label"> Email</label>
              <input
                type="text"
                id="Email"
                className="form-control"
                value={RespondenModel.Email}
               onChange={changeHandler("Email")}
              />
              <br />
              <label className="form-label"> Umur</label>
              <input
                type="text"
                id="Umur"
                className="form-control"
                value={RespondenModel.Umur}
               onChange={changeHandler("Umur")}
              />
              <br />
              <label className="form-label"> No. Handphone</label>
              <input
                type="text"
                id="NoHp"
                className="form-control"
                value={RespondenModel.NoHp}
               onChange={changeHandler("NoHp")}
              />
              <br />
              <label className="form-label"> Status</label>
              <input
                type="text"
                id="Status"
                className="form-control"
                value={RespondenModel.Status}
               onChange={changeHandler("Status")}
              />
               
              <br />
              <label className="form-label"> Kelurahan</label>
              <input
                type="text"
                id="Kelurahan"
                className="form-control"
                value={RespondenModel.Kelurahan}
               onChange={changeHandler("Kelurahan")}
              />
              <br />
              <label className="form-label"> Kecamatan</label>
              <input
                type="text"
                id="Kecamatan"
                className="form-control"
                value={RespondenModel.Kecamatan}
               onChange={changeHandler("Kecamatan")}
              />
              <br />
              <label className="form-label"> Kabupaten</label>
              <input
                type="text"
                id="Kabupaten"
                className="form-control"
                value={RespondenModel.Kabupaten}
               onChange={changeHandler("Kabupaten")}
              />
              <br />
              <label className="form-label"> Fakultas</label>
              <input
                type="text"
                id="Fakultas"
                className="form-control"
                value={RespondenModel.Fakultas}
               onChange={changeHandler("Fakultas")}
              />
              <br />
              <label className="form-label"> Prodi</label>
              <input
                type="text"
                id="Prodi"
                className="form-control"
                value={RespondenModel.Prodi}
               onChange={changeHandler("Prodi")}
              />
              <br />
              <label className="form-label"> Gejala</label>
              <input
                type="text"
                id="Gejala"
                className="form-control"
                value={RespondenModel.Gejala}
               onChange={changeHandler("Gejala")}
              />
              <br />
              <label className="form-label"> Riwayat Perjalanan</label>
              <input
                type="text"
                id="Perjalanan"
                className="form-control"
                value={RespondenModel.Perjalanan}
               onChange={changeHandler("Perjalanan")}
              />
              <br />
              <label className="form-label"> Riwayat Paparan</label>
              <input
                type="text"
                id="Paparan"
                className="form-control"
                value={RespondenModel.Paparan}
               onChange={changeHandler("Paparan")}
              />
              <br />
              <label className="form-label"> Transportasi</label>
              <input
                type="text"
                id="Transportasi"
                className="form-control"
                value={RespondenModel.Transportasi}
               onChange={changeHandler("Transportasi")}
              />
              <br />

            </div>
          </Modal.Body>
          <Modal.Footer>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-primary"
                onClick={onSave}
              >
                Save change{" "}
              </button>
              <button
                type="button"
                className="btn btn-secondary"
                onClick={cancelHandler}
              >
                Close{" "}
              </button>
            </div>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
export default FormResponden;
