import React from 'react';
import FormInput from './formleave'
import Table from 'react-bootstrap/Table';
import Select from 'react-select';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css'
import LeaveService from '../service/LeaveService'
import Modal from 'react-bootstrap/Modal';
import Dropdown from 'react-bootstrap/Dropdown';
import { Form, FormGroup, ControlLabel, FormControl } from 'react-bootstrap'
import { config } from '../config/config';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";


class Leave extends React.Component {
    LeaveModel = {
        id: 0,
        created_by: "",
        created_on: "",
        is_delete: false,
        leave_name_id: "",
        start: "",
        end: "",
        reason: "",
        leave_contact: "",
        leave_address: "",
        leave_type: "",
        leave_name: "",
        x: true,
    }

    QuotaModel = {
        previous: 0,
        reguler: 0,
        collective: 0,
        taken: 0,
        remaining: 0,
    }

    constructor() {
        super();
        this.state = {
            LeaveModel: this.LeaveModel,
            ListLeave: [],
            openmodalInput: false,
            openmodalDel: false,
            mode: "",
            typeOption: [],
            namaOption: [],
            filter: {
                order: '',
                page: '1',
                pagesize: '10',
                tipe: ''
            },
            errors: {},
            totaldata: 1,
            QuotaModel: this.QuotaModel,
            ListQuota: [],
            JumlahTaken: []
        }
    }

    componentDidMount() {
        const { filter } = this.state;
        this.loadList(filter);
        // this.loadQuota();
    }

    // componentDidUpdate() {
    //     const { filter } = this.state;
    //     this.loadList(filter);

    // }

    loadList = async (filter) => {
        const { totaldata } = this.state;
        const respon = await LeaveService.getAll(filter);
        const coundata = await LeaveService.countleave(filter);
        this.getOptionType();
        this.getOptionName();
        if (respon.success) {
            this.setState({
                ListLeave: respon.result,
                totaldata: Math.ceil(coundata.result[0].totaldata / filter.pagesize)
            })
            console.log(totaldata)
        }
        const respons = await LeaveService.getQuota();
        if (respons.success) {
            this.setState({
                ListQuota: respons.result
            })
        };
    }

    handleTampil = () => {
        this.getOptionType();
        this.getOptionName();
        this.setState({
            openmodalInput: true,
            LeaveModel: this.LeaveModel,
            mode: "create"
        })
    }

    cancelHandler = async () => {
        //const respon = await LeaveService.getAll();

        //if (respon.success) {
        this.setState({
            // ListLeave: respon.result,
            openmodalInput: false,
            openmodalDel: false,
            errors: {}
        })
        // }
    }

    getOptionType = async () => {
        const respons = await LeaveService.getType();
        this.setState({
            typeOption: respons.result
        })
    }

    getOptionName = async () => {
        const respons = await LeaveService.getName();
        this.setState({
            namaOption: respons.result
        })
    }

    selectedType = (selectedOption) => {
        console.log(selectedOption.label)

        if (selectedOption.label === "Cuti Khusus") {
            this.setState({
                LeaveModel: {
                    ...this.state.LeaveModel,
                    leave_type: selectedOption.label,
                    x: false,
                }
            })
        }
        else {
            this.setState({
                LeaveModel: {
                    ...this.state.LeaveModel,
                    leave_type: selectedOption.label,
                    x: true,
                }
            })
        }
    }

    selectedName = (selectedOption) => {
        console.log(selectedOption.value)
        this.setState({
            LeaveModel: {
                ...this.state.LeaveModel,
                leave_name_id: selectedOption.value,
                name: selectedOption.label
            }
        })
    }

    changeHandler = val => ({ target: { value } }) => {
        this.setState({
            LeaveModel: {
                ...this.state.LeaveModel,
                [val]: value,
                created_by: localStorage.getItem(config.id)
            }
        })
    }

    saveLeave = async () => {
        const { LeaveModel, mode, filter } = this.state;
        if (this.handleValidation()) {
            const response = await LeaveService.cekDate(LeaveModel);
            if (response.success) {
                if (response.result === undefined) {
                    if (mode === "create") {
                        const respons = await LeaveService.postLeave(LeaveModel);
                        if (respons.success) {
                            alert('Success : ' + respons.result)
                            // this.loadList(filter);
                        }
                        else {
                            alert('Error : ' + respons.result)
                        }
                        this.setState({
                            openmodalInput: false

                        })
                    }
                    else {
                        //fungsi edit
                        const respons = await LeaveService.updateLeave(LeaveModel);
                        const { filter } = this.state;
                        if (respons.success) {
                            alert('Success : ' + respons.result)
                            //this.loadList(filter);
                        }
                        else {
                            alert('Error : ' + respons.result)
                        }
                        this.setState({
                            openmodalInput: false
                        })
                    }
                    this.componentDidMount();
                }
                else {
                    alert('Tanggal Cuti sudah diambil')
                }
            }
        }
        else {
            alert("Form has errors.")
        }
    }

    handleDelete = async (id) => {
        const respon = await LeaveService.getleavebyid(id);
        console.log(respon)
        if (respon.success) {
            this.setState({

                openmodalDel: true,
                LeaveModel: respon.result,

            })

        }
        else {
            alert('Error:' + respon.result)
        }

    }

    sureDelete = async (item) => {
        const { LeaveModel, filter } = this.state;
        const respons = await LeaveService.delete(LeaveModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList(filter);

        }
        else {
            alert('Error : ' + respons.result)
        }
        // this.componentDidMount();
        this.setState({
            openmodalDel: false
        })

    }

    handleEdit = async (id) => {

        const respon = await LeaveService.getleavebyid(id);
        console.log(respon)
        this.getOptionType();
        this.getOptionName();
        if (respon.success) {
            this.setState({
                x: false,
                openmodalInput: true,
                LeaveModel: respon.result,
                mode: "edit"
            })
        }
        else {
            alert('Error:' + respon.result)
        }
        //this.componentDidMount();
    }

    // filterHandler = (val) => {
    //     const { filter } = this.state;
    //     this.setState({
    //         filter: {
    //             ...this.state.filter,
    //             ["tipe"]: val
    //         }
    //     })
    //     console.log(filter)
    //     this.componentDidMount();
    // }

    handlerSorting = () => {
        let order = "";
        const { filter } = this.state;
        if (filter.order === "") {
            order = "DESC"
            alert("DESC")
        }
        else {
            alert("ASC")
        }
        this.setState({
            filter: {
                ...this.state.filter,
                ["order"]: order
            },

        },
            () => this.loadList(this.state.filter))
        //console.log(filter)

        this.componentDidMount();
    }

    onChangePage = (number) => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["page"]: number
            }
        },
            () => this.loadList(this.state.filter));
        //this.componentDidMount();
    }

    renderPagination() {
        let items = [];
        const { filter, totaldata } = this.state;
        for (let number = 1; number <= totaldata; number++) {
            items.push(
                <PaginationItem key={number} active={number === filter.page}>
                    <PaginationLink onClick={() => this.onChangePage(number)} next>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        }
        return (
            <Pagination>{items}</Pagination>
        )
        this.componentDidMount();
    }

    pageSizeHandler = (val) => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["pagesize"]: val
            }
        },
            () => this.loadList(this.state.filter));
        this.componentDidMount();
    }

    filterType = (selectedOption) => {
        console.log(selectedOption.label)
        this.setState({
            filter: {
                ...this.state.filter,
                ["tipe"]: selectedOption.label
            }
        }
            ,
            () => this.loadList(this.state.filter))
        this.componentDidMount();
    }

    handleValidation() {
        let fields = this.state.LeaveModel;
        let errors = {};
        let formIsValid = true;

        //Jenis dan Nama
        if (fields["leave_type"] === "Cuti Khusus") {
            if (!fields["name"]) {
                formIsValid = false;
                errors["name"] = "Nama Tidak Boleh Kosong";
            }
            else if (fields["name"] === "-") {
                formIsValid = false;
                errors["name"] = "Nama Tidak Boleh Kosong";
            }
        }
        else if (fields["leave_type"] === "Cuti Tahunan") {

        }
        else {
            formIsValid = false;
            errors["leave_type"] = "Tipe Tidak Bolek Kosong";
        }

        //Reason
        if (!fields["start"]) {
            formIsValid = false;
            errors["start"] = "start Cannot be empty";
        }

        //End
        if (!fields["end"]) {
            formIsValid = false;
            errors["end"] = "end Cannot be empty";
        }

        //validasi startend
        if (fields["start"] > !fields["end"]) {
            formIsValid = false;
            errors["waktu"] = "Format Waktu Salah";
        }

        //Reason
        if (!fields["reason"]) {
            formIsValid = false;
            errors["reason"] = "reason Cannot be empty";
        }

        //Kontak
        if (!fields["leave_contact"]) {
            formIsValid = false;
            errors["leave_contact"] = "Kontak Cannot be empty";
        }

        //Reason
        if (!fields["leave_address"]) {
            formIsValid = false;
            errors["leave_address"] = "alamat Cannot be empty";
        }

        this.setState({
            errors: errors
        });
        console.log(errors)
        return formIsValid;
    }

    // loadQuota = async () => {

    //     const respons = await LeaveService.getTaken();
    //     if (respons.success) {
    //         this.setState({
    //             JumlahTaken: respons.result
    //         })
    //     };

    //     const { JumlahTaken, ListQuota } = this.state;
    //     const response = await LeaveService.postTaken(JumlahTaken, ListQuota);
    //     if (response.success) {
    //         alert('Success : ' + response.result)
    //         //this.loadList();

    //     }
    //     else {
    //         alert('Error : ' + respons.result)
    //     }

    //     const respon = await LeaveService.getQuota();
    //     if (respon.success) {
    //         this.setState({
    //             ListQuota: respon.result
    //         })
    //     };

    // }

    handleCek = async () => {
        const response = await LeaveService.cekDate();
        if (response.success) {
            if (response.result === undefined) {
                alert('Kosong')
            }
            else {
                alert('Ada Isinya' + response.result)
            }
        }
        else {
            alert('Gagal ceknya')
        }
    }


    render() {
        const {
            ListLeave,
            LeaveModel,
            openmodalInput,
            openmodalDel,
            typeOption,
            namaOption,
            filter,
            errors,
            mode,
            ListQuota, JumlahTaken

        } = this.state;

        let pilihanType = typeOption.map(function (item) {
            return { value: item.id, label: item.TipeLeave }
        });
        // let pilihanName = namaOption.map(function (item) {
        //     return { value: item.id, label: item.name }
        // });

        return (
            <div>


                <FormInput
                    LeaveModel={LeaveModel}
                    openmodalInput={openmodalInput}
                    cancelHandler={this.cancelHandler}
                    typeOption={typeOption}
                    namaOption={namaOption}
                    selectedType={this.selectedType}
                    selectedName={this.selectedName}
                    changeHandler={this.changeHandler}
                    saveLeave={this.saveLeave}
                    openmodalDel={openmodalDel}
                    errors={errors}
                    mode={mode}
                />
                <br />
                <div className='box box-primary box-solid'>
                    <div class="box-header with-border">
                        <h5 class="box-title">Leave Request</h5>
                    </div>
                    <div class="box-body">
                        <div className="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div>
                                        <Select
                                            onChange={this.filterType}
                                            value={this.state.value}
                                            options={pilihanType}
                                            placeholder="-Pilih Filter-" />
                                    </div>
                                </div>
                            </div>

                            <div class="margin ">
                                <div class="btn-group">
                                    <Button
                                        variant="primary" onClick={this.handlerSorting}>
                                        <i class="fa fa-sort"></i>
                                    </Button>
                                </div>
                    &emsp;
                                <div class="btn-group">
                                    <Dropdown>
                                        <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                            <i class="fa fa-list"></i>
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#" onClick={() => this.pageSizeHandler('5')}>5</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={() => this.pageSizeHandler('10')}>10</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={() => this.pageSizeHandler('20')}>20</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={() => this.pageSizeHandler('30')}>30</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </div>
                    &emsp;
                                <div class="btn-group">
                                    <Button
                                        variant="primary" onClick={this.handleTampil}>
                                        <i class="fa fa-plus-circle"></i></Button>
                                </div>

                            </div>
                        </div>
                        <br />
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Data Cuti</h3>
                            </div>
                            <div class="box-body">
                                <div classname="margin">
                                    <div class="text-navy">Previous Year Quota = {ListQuota.previous_year}</div>

                                    <div class="text-navy">Regular Leave Quota = {ListQuota.regular_quota}</div>

                                    <div class="text-navy">Annual Colective Leave = {ListQuota.annual_collective_leave}</div>

                                    <div class="text-navy">Leave Already Taken = {ListQuota.leave_already_taken}</div>

                                    <div class="text-navy">Total Leave Remain = {ListQuota.total_leave_remaining}</div>
                                </div>
                            </div>
                        </div>



                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Jenis Cuti</th>
                                    <th>Nama Cuti</th>
                                    <th>Tanggal Cuti</th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    ListLeave.map(cuti => {
                                        return (
                                            <tr>
                                                <td>{cuti.leave_type}</td>
                                                <td>{cuti.name}</td>
                                                <td>{cuti.start} - {cuti.end}</td>
                                                <td>
                                                    <Dropdown>
                                                        <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                                            More
                                                        </Dropdown.Toggle>

                                                        <Dropdown.Menu>
                                                            <Dropdown.Item href="#" onClick={() => this.handleEdit(cuti.id)}>Edit</Dropdown.Item>
                                                            <Dropdown.Item href="#" onClick={() => this.handleDelete(cuti.id)}>Delete</Dropdown.Item>
                                                        </Dropdown.Menu>
                                                    </Dropdown>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </Table>

                        {this.renderPagination()}
                    </div>
                </div>
                <Modal show={openmodalDel} style={{ opacity: 1 }}>
                    <Modal.Header style={{ backgroundColor: "red" }}>
                    <i class="fa fa-fw fa-trash" style={{color: 'white'}} size="5x"></i>
                        <div class="text-white">Hapus ? </div>
                    </Modal.Header>
                    <Modal.Body>
                    
                        <div class="container">
                                   Anda Yakin Ingin Menghapus Pengajuan Cuti Tanggal   
                                   <strong>{LeaveModel.start}  </strong> 
                                   ?
                        </div>

                    </Modal.Body>
                    <Modal.Footer>

                        <div class="btn-group">
                            <button type="button" className="btn bg-orange" onClick={this.cancelHandler}  >  Tidak </button>
                            <button type="button" className="btn bg-red" onClick={() => this.sureDelete(LeaveModel.id)}> Ya </button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div >

        )

    }


}
export default Leave