import React from 'react';
import { Link } from 'react-router-dom';
export default class Sidebar extends React.Component {
  render() {
    return (

      <div>
        <aside class="main-sidebar">
          {/* <!-- sidebar: style can be found in sidebar.less --> */}
          <section class="sidebar">
            {/* <!-- Sidebar user panel --> */}
            <div class="user-panel">
              <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
              </div>
              <div class="pull-left info">
                <p>Otrinanda Gandhi</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
              </div>
            </div>
            {/* <!-- search form --> */}
            <form action="#" method="get" class="sidebar-form">
              <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </form>
            {/* <!-- /.search form --> */}
            {/* <!-- sidebar menu: : style can be found in sidebar.less --> */}
            <ul class="sidebar-menu" data-widget="tree">
              <li class="header">MAIN NAVIGATION</li>
              <li class="active treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class="active"><a href='/Home' ><i class="fa fa-circle-o"></i> Home </a></li>
                  {/* <li><a href='/User'><i class="fa fa-circle-o"></i> User </a></li>
                  <li><a href='/Barang'><i class="fa fa-circle-o"></i> Barang </a></li>
                  <li><a href='/Order'><i class="fa fa-circle-o"></i> Order </a></li>  
                  <li><a href='/Timesheet'><i class="fa fa-circle-o"></i> Timesheet </a></li>
                  <li><a href='/Loker'><i class="fa fa-circle-o"></i> Sumber Loker </a></li>
                  <li><a href='/Leave'><i class="fa fa-circle-o"></i> Leave Request</a></li>*/}
                  <li><a href='/Category'><i class="fa fa-circle-o"></i> Survey</a></li>
                  {/* <li><a href='/'><i class="fa fa-circle-o"></i> Keluar </a></li> */}
                </ul>
              </li>
            </ul>
          </section>
        </aside>
      </div>
    )
  }
}