import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
class FormInputUser extends React.Component {

    render() {
        const { handleTampil, changeHendler, checkedHendler, UserModel, onSave,   cancelHandler, mode ,openmodal,errors} = this.props;
        

        return (
            <div>

                <Button variant="primary" onClick={handleTampil}> Pilih Barang </Button><br></br>
                
                    <Modal show={openmodal} style={{ opacity: 1 }}>
                        <Modal.Header style={{ backgroundColor: "lightblue" }}>
                            <Modal.Title> {mode} </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          
                            <div className="form-inside-input">
                                <label className="form-label"> Nama</label>
                                <input type="text" id='Nama'
                                    className="form-control" value={UserModel.Nama}
                                    onChange={changeHendler("Nama")} />
                                        <span style={{color: "red"}}>{errors["Nama"]}</span>
                                <br />
                                <label className="form-label"> Email </label>
                                <input type="text" id='Email'
                                    className="form-control" value={UserModel.Email}
                                    onChange={changeHendler("Email")} />
                                         <span style={{color: "red"}}>{errors["Email"]}</span> 
                                <br />
                                <label className="form-label"> Password</label>
                                <input type="text" id='Password'
                                    className="form-control" value={UserModel.Password}
                                    onChange={changeHendler("Password")} />

                                <br />
                                <label className="form-check-label" htmlFor="Active"> Active</label>
                                <br/>
                                <input type="checkbox" id='Active'
                                    className="form-check-input" checked={UserModel.Active} 
                                    onChange={checkedHendler('Active')} />
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <div className='modal-footer'>
                                <button type="button" className= "btn btn-primary" onClick={onSave}>Save change </button>
                                <button type="button" className= "btn btn-secondary" onClick={cancelHandler}>Close </button>
                            </div>
                        </Modal.Footer>
                </Modal>
            </div>

        
     )
    }
}
export default FormInputUser