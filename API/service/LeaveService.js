module.exports = exports = (server, pool) => {

    server.post('/api/getleave', (req, res) => {
        //console.log(req.body)

        const { order, page, pagesize, tipe } = req.body;

        let qFilter =
            (tipe == "Cuti Tahunan") ? `AND k."leave_type"='Cuti Tahunan'` :
                (tipe == "Cuti Khusus") ? `AND k."leave_type"='Cuti Khusus'` :
                    ` `;
        let qOrder = order != "" ? ` ORDER BY m."start" Desc` : 'ORDER BY "start"';
        let perpage = (page - 1) * pagesize;
        var query = `SELECT 
		m.id,m.created_by, m.created_on,m.is_delete, 
        m.leave_name_id, 
		TO_CHAR(m."start" :: DATE, 'dd Monthyyyy') as start, 
		TO_CHAR(m."end" :: DATE, 'dd Monthyyyy') as "end", 
        m.reason, m.leave_contact, m.leave_address ,
		m.leave_name_id,k.leave_type,k.name
        FROM public.x_leave_request as m
		join public.x_leave_name as k
		on m."leave_name_id" = k."id"
        where m.is_delete='false' 
         ${qFilter}
         ${qOrder}
         LIMIT ${pagesize} OFFSET ${perpage};`;
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })

                }
            }
        )
        console.log(query)
    });

    server.get('/api/gettype', (req, res) => {
        var query = `select * from public."DatabaseType"`;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
        //console.log(query);
    });

    server.get('/api/getname', (req, res) => {
        var query = `	select "name","id" from  public.x_leave_name
        where "leave_type" = 'Cuti Khusus'`;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
        //console.log(query);
    });

    server.post('/api/leavepost', (req, res) => {
        console.log(req.body)
        const {
            id,
            created_by,
            created_on,
            is_delete,
            leave_name_id,
            start,
            end,
            reason,
            leave_contact,
            leave_address,
            leave_type,
            leave_name
        } = req.body;
        let qNamaCUti = leave_name_id != "" ? `'${leave_name_id}'` : `'2'`;
        var query = `INSERT INTO public.x_leave_request(
            created_by, created_on,  is_delete, 
            leave_name_id, start, "end", 
            reason, leave_contact, leave_address 
            )
            VALUES 
            ( '${created_by}', current_timestamp, 'false', 
            ${qNamaCUti}, '${start}', '${end}', 
            '${reason}', '${leave_contact}', '${leave_address}' 
            );`
        pool.query(query,

            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${start} berhasil disimpan`
                    })
                }
            })
        console.log(query)
    });

    server.get('/api/getleavebyid/:id', (req, res) => {
        const id = req.params.id;
        var query = `SELECT 
		m.id,m.created_by, m.created_on,m.is_delete, 
        m.leave_name_id, 
		TO_CHAR(m."start" :: DATE, 'yyyy-MM-dd') as start, 
		TO_CHAR(m."end" :: DATE, 'yyyy-MM-dd') as "end", 
        m.reason, m.leave_contact, m.leave_address ,
		m.leave_name_id,k.leave_type,k.name
        FROM public.x_leave_request as m
		join public.x_leave_name as k
		on m."leave_name_id" = k."id"
                    where m."id"='${id}'`
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]

                })
            }
        })
        console.log(query)
    });

    server.put('/api/deleteleave/:id', (req, res) => {
        const id = req.params.id;
        const { created_by } = req.body;

        var query =`UPDATE public.x_leave_request
                    SET 
                        deleted_by='${created_by}', 
                        deleted_on=current_timestamp, 
                        is_delete='true'
                    WHERE "id"='${id}';`

        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${id} berhasil dihapus`
                    });
                }
            }
        )
    });

    server.put('/api/updateLeave/:id', (req, res) => {
        const id = req.params.id;
        const {
            tipe,
            created_by,
            created_on,
            is_delete,
            leave_name_id,
            start,
            end,
            reason,
            leave_contact,
            leave_address,
            leave_type,
            leave_name
        } = req.body;
        let qNamaCUti = 
        (leave_type == "Cuti Khusus") ? `'${leave_name_id}'` 
        :
        (leave_type == "Cuti Tahunan") ? `'2'` 
        : ``
        ;
        var query = `UPDATE public.x_leave_request
                        SET 
                            modified_by='${created_by}', 
                            modified_on=current_timestamp,
                            start='${start}', 
                            "end"='${end}', 
                            reason='${reason}', 
                            leave_contact='${leave_contact}', 
                            leave_address='${leave_address}',  
                            leave_name_id=${qNamaCUti}
                    WHERE "id"  =  '${id}';`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${start} berhasil diubah`
                    });
                }
            }
        )
        console.log(query)
    });

    server.post('/api/countleave', (req, res) => {

        const { date1, date2, order, page, pagesize } = req.body;

       
        var query = `SELECT count(id) as totaldata from 
                         public.x_leave_request
                         where"is_delete"='false' `;
                         
       // console.log(query);
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }

        )

    });

    server.get('/api/getquota', (req, res) => {

        pool.query(`select 
        "id",
        "previous_year",
        "regular_quota",
        "annual_collective_leave",
        "leave_already_taken" ,
        ("regular_quota" + "previous_year" - "annual_collective_leave" - "leave_already_taken") as total_leave_remaining 
        from public.x_employee_leave`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]
                    })
                }
            }
        )
        //console.log(`Get Data Cuti ! `)
    });

    server.post('/api/cekdate', (req, res) => {
        const { start ,end,id} = req.body;


        var query =`select id from public.x_leave_request
        where ("start" between'${start}' and '${end}' or "end" between'${start}' and '${end}')
        and is_delete='false'
        and id !='${id}' `
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]
                    })
                }
            }
        )
        console.log(query)
    });

    // server.get('/api/gettaken', (req, res) => {

    //     pool.query(`select SUM (((date_part('day',age("start", "end")))*'-1')+'1') as total_cuti
    //     from public.x_leave_request where is_delete='false'`,
    //         (error, result) => {
    //             if (error) {
    //                 res.send(400, {
    //                     success: false,
    //                     result: error
    //                 })
    //             }
    //             else {
    //                 res.send(200, {
    //                     success: true,
    //                     result: result.rows[0]
    //                 })
    //             }
    //         }
    //     )
    //     console.log(req.body)
    // });

    // server.post('/api/posttaken', (req, res) => {
    //     //console.log(req.body)

    //     const { id,total_cuti } = req.body;
    //     var query = `UPDATE public.x_employee_leave
    //     SET leave_already_taken ='${total_cuti}'
    //     WHERE "id"='1';`;
    //     pool.query(query,
    //         (error, result) => {
    //             if (error) {
    //                 res.send(400, {
    //                     success: false,
    //                     result: error
    //                 })
    //             }
    //             else {
    //                 res.send(200, {
    //                     success: true,
    //                     result: result.rows
    //                 })

    //             }
    //         }
    //     )
    //     console.log(`UPDATE public.x_employee_leave
    //     SET leave_already_taken ='${total_cuti}'
    //     WHERE "id"='1';`)
    // });

}