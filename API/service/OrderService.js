module.exports = exports = (server, pool) => {
  
    
 
    server.post('/api/orderpost', (req, res) => {
        //console.log(req.body);

        const {ListOrder} = req.body;
        getKodeTransaksi(KodeTransaksi => {
            console.log(KodeTransaksi);
            let TotalHarga = 0, TotalQty = 0;
            let detQuery = '';
            
            
            let id_Customer = ListOrder[0].id_Customer;

            ListOrder.forEach(data => {
                TotalHarga += (data.Harga * data.qtybeli);
                // TotalQty += parseFloat(data.Qty);
                detQuery += detQuery.length > 0 ? `,` : ``;
                detQuery += `((SELECT "Id_OrderHeader" FROM insert1), 
                '${data.id}', ${data.qtybeli}, ${TotalHarga},  false)`

            });

            var query = `WITH insert1 AS (` +
                `INSERT INTO "OrderHeader" ("Kode_Transaksi", "Id_Customer", 
                "isCheckout", "isDelete","Created_By", "Created_Date") ` +
                `VALUES ('${KodeTransaksi}', ${id_Customer}, 
                false, true, ${id_Customer}, current_timestamp) 
                RETURNING "Id_OrderHeader" AS "Id_OrderHeader" ` +
                `), insert2 AS (` +
                `INSERT INTO "OrderDetail" ("Id_OrderHeader", "Id_Barang", 
                "Quantity", "Harga", "isDelete") ` +
                `VALUES ${detQuery} ` +
                `) SELECT * FROM insert1`;

            //  console.log(query);

            pool.query(query, (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: {
                            detail: error.detail
                        }
                    })
                } else {
                    res.send(201, {
                        success: true,
                        result: "Terimakasih sudah  belanja di TokoSAYA"
                    })
                }
            });
        });

    });   

    function getKodeTransaksi(callback) {
        var query = `SELECT "Kode_Transaksi" FROM "OrderHeader" 
            ORDER BY "Kode_Transaksi" DESC LIMIT 1`;
        var newKodeTransaksi = `TRX-`;
        // console.log('tes')
        pool.query(query, (error, result) => {
            if (error) {
                return {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                }
            } else {
                if (result.rows.length > 0) {
                    // console.log(result.rows[0].Kode_Transaksi)
                    var kodeArr = result.rows[0].Kode_Transaksi.split('-');
                    newKodeTransaksi = newKodeTransaksi + ("0000" + (parseInt(kodeArr[1]) + 1).toString()).slice(-4);
                    return callback(newKodeTransaksi);
                } else {
                    newKodeTransaksi = newKodeTransaksi + "0001";
                    return callback(newKodeTransaksi);
                }
            }
        });
    };

    
    server.get('/api/getAlltransaksi', (req,res) => {

        pool.query(`select a."Id_OrderHeader", 
        a."Kode_Transaksi", count(k."Quantity") as totalquantity, sum(k."Harga") as totalharga
        from "OrderHeader" a join "OrderDetail" k on 
        a."Id_OrderHeader" = k."Id_OrderHeader"
        group by a."Id_OrderHeader"
        order by "Id_OrderHeader";`      
        , (error, result) => {
                if(error){
                    res.send(400,{
                        success:false,
                        result: error
                    })
                }else{
                    res.send(200,{
                        success: true,
                        result: result.rows
                    })
                }
            }
        )
    });
    server.get('/api/getAllbarangorder', (req, res) => {

        pool.query(`select m."Kode_Transaksi", a."Kd_Barang", a."Nama_Barang", k."Quantity", a."Harga"
        from public."Barang" a join public."OrderDetail" k on k."Id_Barang" = a."id"
        join public."OrderHeader" m on k."Id_OrderHeader" = m."Id_OrderHeader";`
            , (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        )
    });
}