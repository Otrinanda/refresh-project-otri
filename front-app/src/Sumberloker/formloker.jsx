import React from 'react';

import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';

class FormLoker extends React.Component {



    constructor() {
        super();
        this.state = {
            selectedOption: {}
        }
    }
    render() {
        const {

            changeHandler,
            openmodalInput,
            LokerModel,
            sumberOption,
            selectedSumber,
            tipeOption,
            selectedTipe,
            handleCancel,
            saveLoker,
            errors,
            mode

        } = this.props;

        let pilihanSumber = sumberOption.map(function (item) {
            return { value: item.id, label: item.SumberLoker }
        });

        let pilihanTipe = tipeOption.map(function (item) {
            return { value: item.id, label: item.TipePelamar }
        });

        return (
            <div>
                <Modal show={openmodalInput} style={{ opacity: 1 }}>
                    <Modal.Header style={{ backgroundColor: "navy" }}>
                        <div class="text-white">Detail Pelamar </div>
                        <br />
                        <div class="text-white">ID = {LokerModel.identity_no}</div>
                    </Modal.Header>
                    <Modal.Body>
                        {/* {JSON.stringify(LokerModel)} */}
                        <div className='form-inside-input'>
                            <div className="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label className='form-label'> Sumber </label>
                                        <Select
                                            onChange={selectedSumber}
                                            value={mode === 'create' ?
                                                this.state.value :
                                                { label: LokerModel.vacancy_source, value: LokerModel.id }}
                                            options={pilihanSumber}
                                            placeholder="-Pilih-" />
                                        <span style={{ color: "red" }}>{errors["vacancy_source"]}</span>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label className='form-label'> Tipe Pelamar </label>
                                        <Select
                                            onChange={selectedTipe}
                                            value={mode === 'create' ?
                                                this.state.value :
                                                { label: LokerModel.candidate_type, value: LokerModel.id }}
                                            options={pilihanTipe}
                                            placeholder="-Pilih-" />
                                            <span style={{ color: "red" }}>{errors["candidate_type"]}</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-40">
                                <label className="form-label"> Nama Event </label>
                                <br />
                                <input type="text" id='event_name'
                                    className="form-control" value={LokerModel.event_name}
                                    onChange={changeHandler("event_name")}
                                    disabled={LokerModel.w} required={LokerModel.x}
                                    class="form-control input-sm"
                                />
                                <span style={{ color: "red" }}>{errors["event_name"]}</span>
                                <br />


                                <label className="form-label"> Nama Carrer Center </label>
                                <br />
                                <input type="text" id='career_center_name'
                                    className="form-control" value={LokerModel.career_center_name}
                                    onChange={changeHandler("career_center_name")}
                                    disabled={LokerModel.x}
                                    class="form-control input-sm"
                                />
                                <span style={{ color: "red" }}>{errors["career_center_name"]}</span>
                                <br />

                                <label className="form-label"> Refferer Name </label>
                                <br />
                                <input type="text" id='referrer_name'
                                    className="form-control" value={LokerModel.referrer_name}
                                    onChange={changeHandler("referrer_name")}
                                    disabled={LokerModel.y}
                                />
                                <span style={{ color: "red" }}>{errors["referrer_name"]}</span>
                                <br />

                                <label className="form-label"> Refferer Mobile Number </label>
                                <br />
                                <input type="text" id='referrer_phone'
                                    className="form-control" value={LokerModel.referrer_phone}
                                    onChange={changeHandler("referrer_phone")}
                                    disabled={LokerModel.y}
                                />
                                <span style={{ color: "red" }}>{errors["referrer_phone"]}</span>
                                <br />


                                <label className="form-label"> Refferer Email </label>
                                <br />
                                <input type="text" id='referrer_email'
                                    className="form-control" value={LokerModel.referrer_email}
                                    onChange={changeHandler("referrer_email")}
                                    disabled={LokerModel.y}
                                />
                                <span style={{ color: "red" }}>{errors["referrer_email"]}</span>
                                <br />

                                <label className="form-label"> Sumber Lain </label>
                                <br />
                                <input type="text" id='other_source'
                                    className="form-control input-sm" value={LokerModel.other_source}
                                    onChange={changeHandler("other_source")}
                                    disabled={LokerModel.z}
                                />
                                <span style={{ color: "red" }}>{errors["other_source"]}</span>
                                <br />

                                <br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-grup">
                                            <label className="form-label"> Penghasilan Terakhir(IDR) </label>
                                            <br />
                                            <input type="text" id='last_income'
                                                className="form-control" value={LokerModel.last_income}
                                                onChange={changeHandler("last_income")}
                                            />
                                            {/* <span style={{ color: "red" }}>{errors["last_income"]}</span> */}
                                            <br />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-grup">
                                            <label className='form-label'> Tgl. Lamaran </label>
                                            <input type="date" className='form-control' id='apply_date' value={LokerModel.apply_date} onChange={changeHandler("apply_date")}
                                            />
                                            <span style={{ color: "red" }}>{errors["apply_date"]}</span>
                                            <br />
                                            <br /><br />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div class="btn-group">
                            <button type="button" className="btn bg-orange" onClick={handleCancel}  >      Batal      </button>
                            <button type="button" className="btn bg-navy" onClick={saveLoker}       >      Simpan     </button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
export default FormLoker;

