import axios from 'axios';
import {config} from '../config/config';
const TimesheetService={

    getTimesheet :(filter)=>{
        const result = axios.post(config.apiUrl+'/gettimesheet',filter)
        .then(response =>{
            return{
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result;
    },
    postSetuju:(item)=>{
        const result = axios.post(config.apiUrl+'/posttimesheetsetuju', item)
        .then(respons => {
            return {
                success: respons.data.success,
                result: respons.data.result
            }
        })
        .catch(error => {
            return {
                success: false,
                result: error
            }
        });
        return result;
    },
    // postTolak:(item)=>{
    //     const result = axios.post(config.apiUrl+'/posttimesheettolak', item)
    //     .then(respons => {
    //         return {
    //             success: respons.data.success,
    //             result: respons.data.result
    //         }
    //     })
    //     .catch(error => {
    //         return {
    //             success: false,
    //             result: error
    //         }
    //     });
    //     return result;
    // },
    
    getdetail: (id)=>{
        const result=axios.get(config.apiUrl+'/getdetail/'+id)
        .then(respons => {
            return {
                success: respons.data.success,
                result: respons.data.result
            }
        })
        .catch(error => {
            return {
                success: false,
                result: error
            }
        });
        return result;
    },

    getTahun: () => {
        const result = axios.get(config.apiUrl + '/gettahun')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    getBulan: () => {
        const result = axios.get(config.apiUrl + '/getbulan')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    submit: (send) => {
        const result = axios.post(config.apiUrl + '/submit', send)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    tolak: (ListApprove) => {
        const result = axios.post(config.apiUrl + '/tolak', ListApprove)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    onApprove: (ListApprove) => {
        const result = axios.post(config.apiUrl + '/onapprove', ListApprove)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getNilai: () => {
        const result = axios.get(config.apiUrl + '/getnilai')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },
    
    cekTimesheet: (filter) => {
        const result = axios.post(config.apiUrl + '/cektimesheet',filter)
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result
    },
}

export default TimesheetService;