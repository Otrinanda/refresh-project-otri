import axios from 'axios';
import {config} from '../config/config';
 const KategoriService ={
    getKategori :()=>{
        const result = axios.get(config.apiUrl+'/getkategori')
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    },
    // post: (item) => {
    //     const result = axios.post(config.apiUrl + '/barangpost', item)
    //         .then(respons => {
    //             return {
    //                 success: respons.data.success,
    //                 result: respons.data.result
    //             }
    //         })
    //         .catch(error => {
    //             return {
    //                 success: false,
    //                 result: error
    //             }
    //         });

    //     return result;
    // },
    // getdatabyid: (id) => {
    //     const result = axios.get(config.apiUrl + '/getbarangbyid/'+id)
    //         .then(respons => {
    //             return {
    //                 success: respons.data.success,
    //                 result: respons.data.result
    //             }
    //         })
    //         .catch(error => {
    //             return {
    //                 success: false,
    //                 result: error
    //             }
    //         });

    //     return result;
    // },
    // deletebarang: (id) => {
    //     const result = axios.get(config.apiUrl + '/deletebarang/'+id)
    //         .then(respons => {
    //             return {
    //                 success: respons.data.success,
    //                 result: respons.data.result
    //             }
    //         })
    //         .catch(error => {
    //             return {
    //                 success: false,
    //                 result: error
    //             }
    //         });

    //     return result;
    // },
    // updatebarang: (item) => {
    //     const result = axios.put(config.apiUrl + '/updatebarang/'+item.id, item)
    //         .then(respons => {
    //             return {
    //                 success: respons.data.success,
    //                 result: respons.data.result
    //             }
    //         })
    //         .catch(error => {
    //             return {
    //                 success: false,
    //                 result: error
    //             }
    //         });

    //     return result;
    // },
    // delete: (item) => {
    //     const result = axios.put(config.apiUrl + '/deletebarang/'+item.id, item)
    //         .then(respons => {
    //             return {
    //                 success: respons.data.success,
    //                 result: respons.data.result
    //             }
    //         })
    //         .catch(error => {
    //             return {
    //                 success: false,
    //                 result: error
    //             }
    //         });

    //     return result;
    // }
    
}
export default KategoriService;