server.get('/api/getquota', (req, res) => {

    pool.query(`select 
    "id",
    "previous_year",
    "regular_quota",
    "annual_collective_leave",
    "leave_already_taken" ,
    ("regular_quota" + "previous_year" - "annual_collective_leave" - "leave_already_taken") as total_leave_remaining 
    from public.x_employee_leave`,
        (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        }
    )
    console.log(`select 
        "id",
        "previous_year",
        "regular_quota",
        "annual_collective_leave",
        "leave_already_taken" ,
        ("regular_quota" + "previous_year" - "annual_collective_leave" - "leave_already_taken") as total_leave_remaining 
        from public.x_employee_leave`)
});

server.get('/api/gettaken', (req, res) => {

    pool.query(`select SUM (((date_part('day',age("start", "end")))*'-1')+'1') as total_cuti
    from public.x_leave_request`,
        (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        }
    )
    console.log(req.body)
});

server.post('/api/posttaken', (req, res) => {
    //console.log(req.body)
    
    const { id,total_cuti } = req.body;
    var query = `UPDATE public.x_employee_leave
    SET leave_already_taken ='${total_cuti}'
    WHERE "id"='1';`;
    pool.query(query,
        (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })

            }
        }
    )
    console.log(query)
});