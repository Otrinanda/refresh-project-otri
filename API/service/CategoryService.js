const { Query } = require("pg");

module.exports = exports =(server,pool)=>{

    server.get('/api/getall',(req,res)=>{
        const query=' SELECT * FROM public."Responden" where "Active"=true'
    
        pool.query(query,(error,result)=>{
            if(error){
                res.send(400,{
                    success:false,
                    result:error
                })
            }
            else{
                res.send(200,{
                    success:true,
                    result:result.rows
                });
            }
        })
        console.log(query)
    });

    server.get('/api/getrespondenbyid/:Id', (req, res) => {
        const Id = req.params.Id;
        const query=`SELECT * FROM public."Responden"
                    where "Id"=${Id}`

        pool.query(query
        , (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                    
                })
            }
        }
        )
        console.log(query)
    });

    server.post('/api/postresponden', (req, res) => {
        console.log(req.body)
        const { Nama,Umur,Email,NoHp,Status,Kelurahan,Kecamatan,Kabupaten,Fakultas,Prodi,Gejala,Perjalanan,Paparan,Transportasi} = req.body;
        const query=`INSERT INTO public."Responden"(
            "Nama", "Umur", "Email", "NoHp", 
           "Status", "Kelurahan", "Kecamatan", "Kabupaten", 
           "Fakultas", "Prodi", "Gejala", "Perjalanan", 
           "Paparan", "Transportasi", "Active", 
           "CreatedBy", "CreatedDate")
           VALUES ('${Nama}', '${Umur}', '${Email}', '${NoHp}',
                   '${Status}','${Kelurahan}', '${Kecamatan}', '${Kabupaten}', 
                   '${Fakultas}', '${Prodi}', '${Gejala}','${Perjalanan}', 
                   '${Paparan}', '${Transportasi}', true, 
                   'system', current_timestamp);`

        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Nama} berhasil disimpan`
                    })
                }
            }
        )
    });

    server.put('/api/updateresponden/:Id', (req, res) => {
        const Id = req.params.Id;
        const {  Nama,Umur,Email,NoHp,Status,Kelurahan,Kecamatan,Kabupaten,Fakultas,Prodi,Gejala,Perjalanan,Paparan,Transportasi} = req.body;
       const query =`UPDATE public."Responden"
                    SET 
                    "Nama"='${Nama}', 
                    "Umur"='${Umur}',
                    "Email"='${Email}',
                    "NoHp"='${NoHp}',
                    "Status"='${Status}',
                    "Kelurahan"='${Kelurahan}',
                    "Kecamatan"='${Kecamatan}',
                    "Kabupaten"='${Kabupaten}',
                    "Fakultas"='${Fakultas}',
                    "Prodi"='${Prodi}',
                    "Gejala"='${Gejala}',
                    "Perjalanan"='${Perjalanan}',
                    "Paparan"='${Paparan}',
                    "Transportasi"='${Transportasi}',             
                    "ModifiedBy"='system', 
                    "ModifiedDate"=current_timestamp
                    WHERE 
                    "Id"  =  '${Id}';`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Nama} berhasil diubah`
                    });
                }
            }
        )
        console.log(query)
    });

    server.put('/api/deleteresponden/:Id', (req, res) => {
        const Id = req.params.Id;
        const { Nama} = req.body;
       const query =`UPDATE public."Responden"
                    SET 
                    "Active"= false,
                    "DeletedBy"='system', 
                    "DeletedDate"=current_timestamp
                    where
                    "Id"  =  '${Id}';`
        console.log(query)
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Nama} berhasil dihapus`
                    });
                }
            }
        )
    });

     server.get('/api/getstatus', (req, res) => {
        const query = `SELECT*FROM  public."Status" `
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                });
            }
        })
        console.log(query)
    });
}