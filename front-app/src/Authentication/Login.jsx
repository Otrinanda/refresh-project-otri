import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthService from '../service/AuthService';
import{config} from '../config/config';
export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      formdata: {
        Email: '',
        Password: ''
      },
      isRequest: false,
      errors: {}

    }
  }
  changeHandler = name => ({ target: { value } }) => {
    this.setState({
      formdata: {
        ...this.state.formdata,
        [name]: value
      }
    })
  }
  onSignIn = async () => {
    const { formdata } = this.state
    const respon = await AuthService.postLogin(formdata);
    if (respon.success) {
      localStorage.setItem(config.username,respon.result.Nama)
      localStorage.setItem(config.id,respon.result.Id)
      localStorage.setItem(config.token,respon.result.token)
      alert('Success : ' + respon.result)
      this.props.history.push('/Home')
    }
    else {
      alert('Data Salah !')
    }
  }



  render() {
    return (
      <div class="login-box">
        <div class="login-logo">
          <a href="../../index2.html"><b>OTRI</b>NANDA</a>
        </div>

        <div class="login-box-body">
          <p class="login-box-msg">Sign in to start your session</p>

          <form >
            <div class="form-group has-feedback">
              <input type="email" class="form-control" placeholder="Email"
                onChange={this.changeHandler("Email")} />
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password"
                onChange={this.changeHandler("Password")} />
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">


              <div class="col-xs-4">
                <button type="button" class="btn btn-primary btn-block btn-flat" onClick={this.onSignIn} >Sign In</button>
              </div>

            </div>
          </form>
        </div>

      </div>
    )
  }
}



