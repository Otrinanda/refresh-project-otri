import React from 'react';

import Select from 'react-select';
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
class FormInput extends React.Component {

    constructor() {
        super();
        this.state = {
            selectedOption: {}
        }
    }
    render() {
        const { handleTampil, changeHendler, checkedHendler, BarangModel, onSave, KategoriOption, cancelHandler,selectedHandler, mode ,openmodal} = this.props;
        //kategoriOption di loop untuk mendapatkan Id untuk value dan Nama Kategori
        let pilihan = KategoriOption.map(function (item) {
            return { value: item.Id, label: item.Nama }
        })

        return (
            <div>

                <Button variant="primary" onClick={handleTampil}> Create</Button><br></br>
                
                    <Modal show={openmodal} style={{ opacity: 1 }}>
                        <Modal.Header style={{ backgroundColor: "lightblue" }}>
                            <Modal.Title> {mode} </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>

                            <div className="form-inside-input">
                                <label className="form-label"> Kode Barang</label>
                                <input type="text" id='Kd_Barang'
                                    className="form-control" value={BarangModel.Kd_Barang}
                                    onChange={changeHendler("Kd_Barang")} />
                                {/* <span style={{ color: 'red' }}>{error["Kd_Barang"]}</span> */}
                                <br />
                                <label className="form-label"> Nama Barang</label>
                                <input type="text" id='Nama_Barang'
                                    className="form-control" value={BarangModel.Nama_Barang}
                                    onChange={changeHendler("Nama_Barang")} />
                                <br />
                                <label className="form-label"> Kategori </label>
                                <Select
                                    
                                    onChange={selectedHandler}
                                    value={mode === 'create' ?
                                        this.state.value :
                                        { label: BarangModel.kategori_nama, value: BarangModel.Kategori_Id }}
                                    options={pilihan}
                                />
                                <label className="form-label"> Qty</label>
                                <input type="text" id='Qty'
                                    className="form-control" value={BarangModel.Qty}
                                    onChange={changeHendler("Qty")} />
                                <br />
                                <label className="form-label"> Harga</label>
                                <input type="text" id='Harga'
                                    className="form-control" value={BarangModel.Harga}
                                    onChange={changeHendler("Harga")} />
                                <br />
                                <label className="form-check-label" htmlFor="Active"> Active</label>
                                <input type="checkbox" id='Active'
                                    className="form-check-input" checked={BarangModel.Active} id="Active"
                                    onChange={checkedHendler('Active')} />
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <div className='modal-footer'>
                                <button type="button" className= "btn btn-primary" onClick={onSave}>Save change </button>
                                <button type="button" className= "btn btn-secondary" onClick={cancelHandler}>Close </button>
                            </div>
                        </Modal.Footer>
                </Modal>
            </div>

        
     )
    }
}
export default FormInput