import React from "react";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.min.css";
import Modal from "react-bootstrap/Modal";
import Dropdown from "react-bootstrap/Dropdown";
import { config } from "../config/config";
import CategoryService from "../service/CategoryService";
import FormResponden from "./FormResponden";

class Category extends React.Component {
  RespondenModel = {
    id: 0,
    Nama: "",
    Email: "",
    Umur:"",
    NoHp:"",
    Status:"",
    Kelurahan:"",
    Kecamatan:"",
    Kabupaten:"",
    Fakultas:"",
    Prodi:"",
    Gejala:"",
    Perjalanan:"",
    Paparan:"",
    Transportasi:"",
    Active: false,
    Created_By: "",
    Created_Date: "",
    Modifed_By: "",
    Modifed_Date: "",
    Deleted_By: "",
    Deleted_Date: ""
  };
  constructor() {
    super();
    this.state = {
      ListResponden: [],
      RespondenModel: this.RespondenModel,
      hidden: true,
      mode: "",
      openmodal: false,
      openmodaldel: false,
      StatusOption:[],
    };
  }

  componentDidMount() {
    this.loadList();
  }

  loadList = async () => {
    const respon = await CategoryService.getAll();
    this.getOptionStatus();
    console.log(respon)
    if (respon.success) {
      this.setState({
        ListResponden: respon.result,
      });
      console.log("berhasil");
    } else {
      console.log("gagal load");
    }
  };

   cancelHandler = async () => {

        this.setState({

          openmodal: false,
        });
    };

    handleTampil = () => {
      this.getOptionStatus();
      this.setState({
        hidden: true,
        openmodal: true,
        RespondenModel: this.RespondenModel,
        mode: "create",
      });
  };

    handleEdit = async (Id) => {
      const respon = await CategoryService.getrespondenbyid(Id);
      console.log(respon);
      this.getOptionStatus();
      if (respon.success) {
        this.setState({
          hidden: false,
          openmodal: true,
          RespondenModel: respon.result,
          mode: "edit",
        });
      } else {
        alert("Error:" + respon.result);
      }
    };


   changeHandler = name => ({ target: { value } }) => {
      this.setState({
        RespondenModel: {
          ...this.state.RespondenModel,
          [name]: value
        },
      });
    };

    onSave = async () => {
      const { RespondenModel, mode } = this.state;
      if (mode === "create") {
          const respons = await CategoryService.post(RespondenModel);
          if (respons.success) {
              alert('Success : ' + respons.result)
              this.loadList();
          }
          else {
              alert('Error : ' + respons.result)
          }
          this.setState({
              hidden: true,
              openmodal: false
          })
      }
      else {
          //fungsi edit
          const respons = await CategoryService.updateResponden(RespondenModel);
          if (respons.success) {
              alert('Success : ' + respons.result)
              this.loadList();
          }
          else {
              alert('Error : ' + respons.result)
          }
          this.setState({
              hidden: true,
              openmodal: false
          })
     }
    };

    handleDelete = async (Id) => {
      const respon = await CategoryService.getrespondenbyid(Id);
      console.log(respon);
      if (respon.success) {
        this.setState({
          openmodaldel: true,
          RespondenModel: respon.result,
        });
      } 
      else {
        alert("Error:" + respon.result);
      }
    };

    sureDelete = async (item) => {
      const { RespondenModel } = this.state;
      const respons = await CategoryService.deleteresponden(RespondenModel);
      console.log(RespondenModel)
      if (respons.success) {
        alert("Success : " + respons.result);
        this.loadList();
      } else {
        alert("Error : " + respons.result);
    }
      this.setState({
        openmodaldel: false,
      });
    };

    getOptionStatus = async () => {
      const respon = await CategoryService.getStatus();
      if(respon.success){
        this.setState({
          StatusOption: respon.result
      })
      console.log(this.state.StatusOption)
      } else{
        alert("Error : " + respon.result);
      }
      
  };
  selectedStatus = (selectedOption) => {
    this.setState({
        StatusModel: {
            ...this.state.StatusModel,
            StatusId: selectedOption.value,
            StatusNama: selectedOption.label
        }
    })
    console.log(this.state.BarangModel)
}
  
//   handleValidation() {
//     let fields = this.state.LeaveModel;
//     let errors = {};
//     let formIsValid = true;

//     //Jenis dan Nama
//     if (fields["leave_type"] === "Cuti Khusus") {
//       if (!fields["name"]) {
//         formIsValid = false;
//         errors["name"] = "Nama Tidak Boleh Kosong";
//       } else if (fields["name"] === "-") {
//         formIsValid = false;
//         errors["name"] = "Nama Tidak Boleh Kosong";
//       }
//     } else if (fields["leave_type"] === "Cuti Tahunan") {
//     } else {
//       formIsValid = false;
//       errors["leave_type"] = "Tipe Tidak Bolek Kosong";
//     }

//     //Reason
//     if (!fields["start"]) {
//       formIsValid = false;
//       errors["start"] = "start Cannot be empty";
//     }

//     //End
//     if (!fields["end"]) {
//       formIsValid = false;
//       errors["end"] = "end Cannot be empty";
//     }

//     //validasi startend
//     if (fields["start"] > !fields["end"]) {
//       formIsValid = false;
//       errors["waktu"] = "Format Waktu Salah";
//     }

//     //Reason
//     if (!fields["reason"]) {
//       formIsValid = false;
//       errors["reason"] = "reason Cannot be empty";
//     }

//     //Kontak
//     if (!fields["leave_contact"]) {
//       formIsValid = false;
//       errors["leave_contact"] = "Kontak Cannot be empty";
//     }

//     //Reason
//     if (!fields["leave_address"]) {
//       formIsValid = false;
//       errors["leave_address"] = "alamat Cannot be empty";
//     }

//     this.setState({
//       errors: errors,
//     });
//     console.log(errors);
//     return formIsValid;
//   }



  render() {
    const {
      ListResponden,
      RespondenModel,
      hidden,
      mode,
      openmodal,
      openmodaldel,
      StatusOption
    } = this.state;

    return (
      <div>
         <FormResponden
                    handleTampil={this.handleTampil}
                   changeHandler={this.changeHandler}
                   //checkedHendler={this.checkedHendler}
                    RespondenModel={RespondenModel}
                    onSave={this.onSave}
                    hidden={hidden}
                    title={mode}
                    mode={mode}
                    openmodal={openmodal}
                    openmodaldel={openmodaldel}
                    cancelHandler={this.cancelHandler}
                    StatusOption={this.StatusOption}
                />  
        <br />
        <div className="box box-primary box-solid">
          <div class="box-header with-border">
            <h5 class="box-title">Responden List</h5>
          </div>
          <div class="box-body">
            <br />

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Active</th>
                </tr>
              </thead>

              <tbody>
                {
                   ListResponden.map(rsp => {
                       return (
                           <tr>
                               <td>{rsp.Id}</td>
                               <td>{rsp.Nama}</td>
                               <td>{rsp.Email}</td>
                               <td>{rsp.Active.toString()}</td>
                               <td>
                                   <Dropdown>
                                       <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                           More
                                       </Dropdown.Toggle>
                                       <Dropdown.Menu>
                                           <Dropdown.Item href="#" 
                                           onClick={() => this.handleEdit(rsp.Id)}
                                           >Edit</Dropdown.Item>
                                           <Dropdown.Item href="#" 
                                           onClick={() => this.handleDelete(rsp.Id)}
                                           >Delete</Dropdown.Item>
                                       </Dropdown.Menu>
                                   </Dropdown>
                               </td>
                           </tr>
                       )
                   })
                }
              </tbody>
            </Table>
          </div>
        </div>
         
                <Modal show={openmodaldel}>
                    <Modal.Body> Anda Yakin Ingin Menghapus Data {RespondenModel.Nama} ? </Modal.Body>
                    <Modal.Footer>
                    <button type="button" className="btn bg-orange" onClick={this.cancelHandler}  >  Tidak </button>
                    <button type="button" className="btn bg-blue" onClick={() => this.sureDelete(RespondenModel.Id)}> Tentu! </button>
                    </Modal.Footer>
                </Modal>
      </div>
    );
  }
}
export default Category;
