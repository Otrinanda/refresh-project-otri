import React from 'react';

import FormInput from './forminput'
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css'
import BarangService from '../service/BarangService'
import Modal from 'react-bootstrap/Modal';
import KategoriService from '../service/KategoriService';

import { config } from '../config/config';
class Barang extends React.Component {
    BarangModel = {
        id: 0,
        Kd_Barang: '',
        Nama_Barang: '',
        Qty: 0,
        Harga: 0,
        Active: false,
        Kategori_Id: "",
        Kategori_Nama: "",
        Created_By: "",
        Created_Date: ""
    }
    constructor() {
        super();
        this.state = {
            ListBarang: [],
            BarangModel: this.BarangModel,
            hidden: true,
            mode: "",
            openmodal: false,
            openmodaldel: false,
            KategoriOption: [],
            Created_By: "gatau"
        }

    }
    componentDidMount() {
        this.loadList();
    }

    loadList = async () => {
        const respon = await BarangService.getAll();

        if (respon.success) {
            this.setState({
                ListBarang: respon.result
            })
        }

    }
    cancelHandler = async () => {
        const respon = await BarangService.getAll();

        if (respon.success) {
            this.setState({
                ListBarang: respon.result,
                openmodal: false
            })
        }

    }
    handleTampil = () => {
        this.getOptionKategori();
        this.setState({
            hidden: true,
            openmodal: true,
            BarangModel: this.BarangModel,
            mode: "create"
        })
    }

    getOptionKategori = async () => {
        const respon = await KategoriService.getKategori();
        this.setState({
            KategoriOption: respon.result
        })
        console.log(this.state.KategoriOption)
    }

    handleEdit = async (id) => {

        const respon = await BarangService.getdatabyid(id);
        console.log(respon)
        this.getOptionKategori();
        if (respon.success) {
            this.setState({
                hidden: false,
                openmodal: true,
                BarangModel: respon.result,
                mode: "edit"
            })
        }
        else {
            alert('Error:' + respon.result)
        }
    }
    handleDelete = async (id) => {
        const respon = await BarangService.getdatabyid(id);
        console.log(respon)
        if (respon.success) {
            this.setState({

                openmodaldel: true,
                BarangModel: respon.result,

            })
        }
        else {
            alert('Error:' + respon.result)
        }
    }
    changeHendler = name => ({ target: { value } }) => {

        this.setState({
            BarangModel: {
                ...this.state.BarangModel,
                [name]: value,
                //deklarasiin crated by samadengan id
                Created_By: localStorage.getItem(config.id)
            }
        })
    }

    selectedHandler = (selectedOption) => {
        this.setState({
            BarangModel: {
                ...this.state.BarangModel,
                Kategori_Id: selectedOption.value,
                kategori_nama: selectedOption.label
            }
        })
        console.log(this.state.BarangModel)
    }
    checkedHendler = name => ({ target: { checked } }) => {

        this.setState({
            BarangModel: {
                ...this.state.BarangModel,
                [name]: checked
            }
        })
    }
    sureDelete = async (item) => {
        const { BarangModel } = this.state;
        const respons = await BarangService.delete(BarangModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList();

        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodaldel: false
        })
    }
    onSave = async () => {
        const { BarangModel, mode } = this.state;
        if (mode === "create") {
            const respons = await BarangService.post(BarangModel);
            if (respons.success) {
                alert('Success : ' + respons.result)
                this.loadList();
            }
            else {
                alert('Error : ' + respons.result)
            }
            this.setState({
                hidden: true,
                openmodal: false
            })
        }
        else {
            //fungsi edit
            const respons = await BarangService.updatebarang(BarangModel);
            if (respons.success) {
                alert('Success : ' + respons.result)
                this.loadList();

            }
            else {
                alert('Error : ' + respons.result)
            }
            this.setState({
                hidden: true,
                openmodal: false
            })
        }
    }

    render() {
        const { ListBarang,
            BarangModel,
            hidden,

            KategoriOption,
            mode,
            openmodal,
            openmodaldel

        } = this.state;
        return (
            <div>
                <h5> List Barang </h5>

                <FormInput handleTampil={this.handleTampil}

                    changeHendler={this.changeHendler}
                    checkedHendler={this.checkedHendler}
                    BarangModel={BarangModel}
                    onSave={this.onSave}
                    hidden={hidden}
                    title={mode}
                    KategoriOption={KategoriOption}
                    selectedHandler={this.selectedHandler}
                    mode={mode}
                    openmodal={openmodal}
                    openmodaldel={openmodaldel}
                    cancelHandler={this.cancelHandler}
                />

                <div class="btn-group">
                    <button type="button" class="btn btn-danger dropdown-toogle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-navicon"></i>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#" >10</a></li>
                        <li><a href="#" >20</a></li>
                        <li><a href="#" >30</a></li>
                        <li><a href="#" >40</a></li>
                        <li><a href="#" >5</a></li>
                    </ul>
                </div>

                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th> Id </th>
                            <th> Kode Barang </th>
                            <th> Nama Barang </th>
                            <th> Qty </th>
                            <th> Harga </th>
                            <th> Active </th>
                            <th> Nama Kategori </th>
                            <th> Created By</th>
                            <th> Created Date</th>
                            <th> Action</th>

                        </tr>
                    </thead>

                    <tbody>
                        {
                            ListBarang.map(brg => {
                                return (
                                    <tr>
                                        <td>{brg.id}</td>
                                        <td>{brg.Kd_Barang}</td>
                                        <td>{brg.Nama_Barang}</td>
                                        <td>{brg.Qty}</td>
                                        <td>{brg.Harga}</td>
                                        <td>{brg.Active.toString()}</td>
                                        <td>{brg.kategori_nama}</td>
                                        <td>{brg.Created_By}</td>
                                        <td>{brg.Created_Date}</td>
                                        <td>
                                            <Button variant='warning' onClick={() => this.handleEdit(brg.id)}>Edit</Button> &emsp;
                                            <Button variant='danger' onClick={() => this.handleDelete(brg.id)}>Delete</Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }

                    </tbody>
                </Table>
                <Modal show={openmodaldel}>
                    <Modal.Body> Anda Yakin Ingin Menghapus Data {BarangModel.Kd_Barang} ? </Modal.Body>
                    <Modal.Footer>
                        <button variant="primary" onClick={() => this.sureDelete(BarangModel.id)}> Tentu! </button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
export default Barang