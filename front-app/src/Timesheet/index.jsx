import React from 'react';
import FormDetail from './formtimesheet';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css'
import TimesheetService from '../service/TimesheetService'
import Modal from 'react-bootstrap/Modal';
import Select from 'react-select';
import  CheckBox  from './CheckBox'



class Timesheet extends React.Component {
    TimesheetModel = {
        id: 0,
        status: "",
        timesheet_date: "",
        placement_id: "",
        activity: "",
        start: "",
        end: "",
        overtime: "",
        start_ot: "",
        end_ot: "",
        tahun: "",
        bulan: "",
        active:false
    }


    constructor() {
        super();
        this.state = {
            TimesheetModel: this.TimesheetModel,
            ListTimesheet: [],
            tahunOption: [],
            bulanOption: [],
            openmodalDetail: false,
            openmodalcoba: false,
            openmodalcobaall: false,
            selectedOption: {},
            ListApprove: [],
            lolos: {},
            filter: {
                tahun: '0',
                bulan: '0'
            },
            openmodalNilai: false,
            nilai: {
                hasil: 0,
                kompetensi: '0',
                disiplin: '0'
            },
            nilaiOption: [],
            x: true,
            Checked: false,
            CheckedX: false
        }
        this.onChangeCheckBox = this.onChangeCheckBox.bind(this);
    }

    componentDidMount() {
        const { filter } = this.state;
        this.loadTimesheet(filter);
        this.getOptionTahun();
        this.getOptionBulan();
        this.setState({
            TimesheetModel: this.TimesheetModel
        })
    }

    // componentDidUpdate() {
    //     const { filter } = this.state;
    //     this.loadTimesheet(filter);

    // }

    loadTimesheet = async (filter) => {
        const respon = await TimesheetService.getTimesheet(filter);
        if (respon.success) {
            this.setState({
                ListTimesheet: respon.result
            })

            const respons = await TimesheetService.cekTimesheet(filter);
            if (respons.success) {
                if (respons.result === undefined) {
                    this.setState({
                        x: true
                    })
                }
                else {
                    this.setState({
                        x: false
                    })
                }
            }
        }

    }

    handleDetail = async (id) => {
        console.log(id)
        const respon = await TimesheetService.getdetail(id);
        if (respon.success) {
            alert('Success getdetail Id=' + id)
            this.setState({
                TimesheetModel: respon.result,
                openmodalDetail: true
            })
        }
    }

    handleCancel = async () => {
        this.setState({
            openmodalDetail: false,
            openmodalNilai: false,
            openmodalcoba: false,
            openmodalcobaall: false,
        })
    }

    selectedTahun = (selectedOption) => {

        this.setState({
            TimesheetModel: {
                ...this.state.TimesheetModel,
                tahun: selectedOption.label
            }
        })
    }

    getOptionTahun = async () => {
        const respon = await TimesheetService.getTahun();
        this.setState({
            tahunOption: respon.result
        })
    }

    selectedBulan = (selectedOption) => {
        this.setState({
            TimesheetModel: {
                ...this.state.TimesheetModel,
                bulan: selectedOption.value
            }
        })
    }

    getOptionBulan = async () => {
        const respon = await TimesheetService.getBulan();
        this.setState({
            bulanOption: respon.result
        })
    }

    toggleChange = () => {
        this.setState({
            Checked: !this.state.Checked,
        });
    }
    toggleChangeX = () => {
        this.setState({
            CheckedX: !this.state.CheckedX,
        });
    }

    


    onSelect = (tmsht) => {
        //alert(JSON.stringify(tmsht))
        const { ListApprove, lolos } = this.state;
        let idx = ListApprove.findIndex(i => i.id === tmsht.id)
        if (idx !== -1) {
            ListApprove.splice(idx, 1);
        }
        else {
            this.setState({
                openmodalcoba: true,
                lolos: {
                    id: tmsht.id,
                    status: tmsht.status,
                    timesheet_date: tmsht.timesheet_date,
                    placement_id: tmsht.placement_id,
                    activity: tmsht.activity,
                    start: tmsht.start,
                    end: tmsht.end,
                    overtime: tmsht.overtime,
                    start_ot: tmsht.start_ot,
                    end_ot: tmsht.end_ot
                }
            })
        }
        //this.onSetCheck();
    }

    onSetCheck = () => {
        const { ListApprove, lolos } = this.state;
        ListApprove.push(lolos);
        //alert(JSON.stringify(ListApprove))
        this.setState({
            openmodalcoba: false
        })
        //this.onMasuk();
    }

    onSelectAll = (ListTimesheet) => {
        alert(JSON.stringify(ListTimesheet))
        this.setState({
            ListApprove: this.state.ListTimesheet
        })

        // alert(JSON.stringify(ListApprove))
        //  this.setState({
        //     openmodalcobaall: true
        // })
    }
    onSetCheckAll = () => {
        const { ListApprove, lolos } = this.state;
        //ListApprove.push(lolos);
        // alert(JSON.stringify(ListApprove))
        //    const ListApprove = {
        //     ListApprove: this.state.ListApprove
        // } 
        this.setState({
            openmodalcobaall: false
        })
        alert(JSON.stringify(ListApprove))
        //this.onMasuk();
    }

    onSave = async () => {
        const ListApprove = {
            ListApprove: this.state.ListApprove
        }
        alert(JSON.stringify(ListApprove))

        this.componentDidMount();
        this.getOptionNilai();
        this.setState({
            openmodalNilai: true
        })

    }

    onSubmit = async () => {
        const { nilai, filter } = this.state;
        const lolos = {
            lolos: this.state.lolos
        }
        const ListApprove = {
            ListApprove: this.state.ListApprove
        }
        alert(JSON.stringify(ListApprove));
        alert(JSON.stringify(nilai));
        const send = {
            nilai: this.state.nilai,
            ListApprove: this.state.ListApprove,
            lolos: this.state.ListApprove[0],
        }
        alert(JSON.stringify(lolos));
        const respons = await TimesheetService.submit(send);
        if (respons.success) {
            alert('Success Submit: ' + respons.result)
            this.loadTimesheet(filter);

            const respon = await TimesheetService.onApprove(ListApprove);
            if (respon.success) {
                alert('Success on Approve: ' + respon.result)

            }
            else {
                alert('Error Approve : ' + respon.result)
            }

        } else {
            alert('Error Submit : ' + respons.result)
        }




        this.getOptionNilai();
        this.setState({
            ListApprove: [],
            nilai: [],
            send: [],
            openmodalNilai: false,
            Checked: false,
            CheckedX: false
        })
        this.componentDidMount();
    }

    filterTahun = (selectedOption) => {
        const { filter } = this.state;
        console.log(selectedOption.label)
        this.setState({
            filter: {
                ...this.state.filter,
                ["tahun"]: selectedOption.label
            }

        },
            () => this.loadTimesheet(this.state.filter))

        //console.log(filter)
        this.componentDidMount();
    }

    filterBulan = (selectedOption) => {
        const { filter } = this.state;
        console.log(selectedOption.value)
        this.setState({
            filter: {
                ...this.state.filter,
                ["bulan"]: selectedOption.value
            },
            // x: false
        },
            () => this.loadTimesheet(this.state.filter))

        //console.log(filter)
        this.componentDidMount();

    }

    getOptionNilai = async () => {
        const respons = await TimesheetService.getNilai();
        this.setState({
            nilaiOption: respons.result
        })
    }

    selectedHasil = (selectedOption) => {
        console.log(selectedOption.value)
        this.setState({
            nilai: {
                ...this.state.nilai,
                hasil: selectedOption.value
            }
        })
    }

    selectedKompetensi = (selectedOption) => {
        console.log(selectedOption.value)
        this.setState({
            nilai: {
                ...this.state.nilai,
                kompetensi: selectedOption.value
            }
        })
    }

    selectedKedisiplinan = (selectedOption) => {
        console.log(selectedOption.value)
        this.setState({
            nilai: {
                ...this.state.nilai,
                disiplin: selectedOption.value
            }
        })
    }
    onTolak = async () => {
        const ListApprove = {
            ListApprove: this.state.ListApprove
        }
        alert(JSON.stringify(ListApprove))
        const respons = await TimesheetService.tolak(ListApprove);
        if (respons.success) {
            alert('Success Tolak : ' + respons.result)

        } else {
            alert('Error : ' + respons.result)
        }
        this.componentDidMount();

        this.setState({
            ListApprove: [],
            Checked: false,
            CheckedX: false
        })
    }

    onReset = () => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["bulan"]: 0,
                ["tahun"]: 0,
            },
            x: true
        })
        this.loadTimesheet(filter);
    }

    onChangeCheckBox(e) {
        this.setState({
          checked: e.target.checked,
        });
      }

    render() {
        const {
            TimesheetModel,
            ListTimesheet,
            openmodalDetail,
            openmodalcoba,
            openmodalcobaall,
            tahunOption,
            bulanOption,
            Checked,
            CheckedX,
            nilaiOption,
            openmodalNilai,
            x,
            ListApprove,
            lolos

        } = this.state;

        let pilihanTahun = tahunOption.map(function (item) {
            return { value: item.Tahun, label: item.Tahun }
        });

        let pilihanBulan = bulanOption.map(function (item) {
            return { value: item.NomerBulan, label: item.NamaBulan }
        });

        let pilihanNilai = nilaiOption.map(function (item) {
            return { value: item.id, label: item.keterangan }
        });

        return (
            <div>

                <br />

                <FormDetail
                    handleDetail={this.handleDetail}
                    handleCancel={this.handleCancel}
                    openmodalDetail={openmodalDetail}
                    TimesheetModel={TimesheetModel}
                    openmodalcoba={openmodalcoba}
                    openmodalcobaall={openmodalcobaall}
                    onSetCheck={this.onSetCheck}
                    onSetCheckAll={this.onSetCheckAll}
                />

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h5 class="box-title">Timesheet Approval</h5>
                    </div>
                    <div class="box-body">
                        <div className="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label className='form-label'> Tahun </label>
                                    <Select
                                        onChange={this.filterTahun}
                                        value={this.state.value}
                                        options={pilihanTahun}
                                        placeholder="-Pilih Tahun-" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label className='form-label'> Bulan </label>
                                    <Select
                                        onChange={this.filterBulan}
                                        value={this.state.value}
                                        options={pilihanBulan}
                                        placeholder="-Pilih Bulan-" />
                                </div>
                            </div>
                        </div>
                        {/* {JSON.stringify(ListTimesheet)} */}
                        <br />
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox"
                                            // className="form-check-input"
                                            onClick={() => this.onSelectAll(ListTimesheet)}
                                            checked={Checked}
                                            onChange={this.toggleChange}
                                        />
                                    </th>
                                    <th>Status</th>
                                    <th> Tanggal </th>
                                    <th> Client </th>
                                    <th> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {ListTimesheet.map(tmsht => {
                                    return (
                                        <tr>
                                            <td class="input-group input-group-lg">

                                                <input type="checkbox"
                                                    // className="form-check-input"
                                                    onClick={() => this.onSelect(tmsht)}
                                                    //checked={CheckedX}
                                                    //onChange={this.toggleChangeX}
                                                    checked={ListApprove.find((ch) => ch.id===tmsht.id)}
                                                    onChange={this.onChangeCheckBox}
                                                />

                                            </td>
                                            <td>{tmsht.status}</td>
                                            <td>{tmsht.timesheet_date}</td>
                                            <td>{tmsht.placement_id}</td>
                                            <td>
                                                <Button variant='light'
                                                    onClick={() => this.handleDetail(tmsht.id)}>
                                                    <i class="fa fa-search"></i>
                                                </Button>
                                            </td>

                                        </tr>
                                    )
                                })}
                            </tbody>
                        </Table>
                        <div class="btn-group" >
                            <button type="button" disabled={x} className="btn bg-orange" onClick={this.onReset}  >      Batal      </button>
                            <button type="button" disabled={x} className="btn bg-red" onClick={this.onTolak}>      Tolak      </button>
                            <button type="button" disabled={x} className="btn bg-navy" onClick={this.onSave}       >      Setujui     </button>
                        </div>
                      
                    </div>
                    <br />
                </div>
                <Modal show={openmodalNilai} style={{ opacity: 1 } } size="lg">
                    <Modal.Header style={{ backgroundColor: "navy" }}>
                        <div class="text-white">Form Penilaian </div>
                    </Modal.Header>
                    <Modal.Body>
                        <h4 class="text-navy"> Time report ini saya buat dengan sungguh sungguh dan sebenarnya sesuai dengan nilai-nilai estika dan profesionalisme perusahaan.</h4>
                        
                        <hr></hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label className='form-label'> Sasaran dan Hasil Kerja </label>
                                    <Select
                                        onChange={this.selectedHasil}
                                        value={this.state.value}
                                        options={pilihanNilai}
                                        placeholder="-Pilih-" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label className='form-label'> Kompetensi Pendukung</label>
                                    <Select
                                        onChange={this.selectedKompetensi}
                                        value={this.state.value}
                                        options={pilihanNilai}
                                        placeholder="-Pilih-" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label className='form-label' > Kedisiplinan</label>

                                    <Select
                                        onChange={this.selectedKedisiplinan}
                                        value={this.state.value}
                                        options={pilihanNilai}
                                        placeholder="-Pilih-" />
                                </div>
                            </div>
                        </div>

                    </Modal.Body>
                    <Modal.Footer>

                        <div class="btn-group">
                            <button type="button" className="btn bg-orange" onClick={this.handleCancel}  >      Batal      </button>
                            <button type="button" className="btn bg-navy" onClick={this.onSubmit}       >      Proses    </button>
                        </div>
                    </Modal.Footer>
                </Modal>

            </div >
        )
    }
};

export default Timesheet
