import React from 'react';
import Select from 'react-select';
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';


class FormInput extends React.Component {

    constructor() {
        super();
        this.state = {

        }
    }

    render() {
        const {
            LeaveModel,
            openmodalInput,
            cancelHandler,
            typeOption,
            namaOption,
            selectedType,
            selectedName,
            changeHandler,
            saveLeave,
            errors,
            mode
        } = this.props

        let pilihanType = typeOption.map(function (item) {
            return { value: item.id, label: item.TipeLeave }
        });

        let pilihanName = namaOption.map(function (item) {
            return { value: item.id, label: item.name }
        });

        return (
            <div>
                <Modal show={openmodalInput} style={{ opacity: 1 }} >
                    <Modal.Header style={{ backgroundColor: "Navy" }}>
                        <div class="text-white">Form Cuti </div>
                    </Modal.Header>
                    <Modal.Body>

                        {/* {JSON.stringify(LeaveModel)} */}
                        <div className='form-inside-input'>
                            <div className="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label className='form-label'> Jenis Cuti </label>
                                        <Select
                                            onChange={selectedType}
                                            value={mode ==='create' ?
                                                this.state.value:
                                            {label:LeaveModel.leave_type, value:LeaveModel.id}}
                                            options={pilihanType}
                                            placeholder="-Pilih-" />
                                        <span style={{ color: "red" }}>{errors["leave_type"]}</span>
                                        <br />

                                        <label className='form-label'> Mulai dari(Awal) </label>
                                        <input type="date"
                                            className='form-control' id='start'
                                            value={LeaveModel.start}
                                            onChange={changeHandler("start")}
                                        />
                                        <span style={{ color: "red" }}>{errors["start"]}</span>
                                        <span style={{ color: "red" }}>{errors["waktu"]}</span>
                                        <br />

                                        <label className='form-label'> Alasan Cuti </label>
                                        <br />
                                        <input type="text" id='reason'
                                            className="form-control" value={LeaveModel.reason}
                                            onChange={changeHandler("reason")}
                                        />
                                        <span style={{ color: "red" }}>{errors["reason"]}</span>
                                        <br />

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label className='form-label'> Nama Cuti </label>
                                        <br />
                                        <Select
                                            onChange={selectedName}
                                            value={mode ==='create' ?
                                                this.state.value:
                                            {label:LeaveModel.name, value:LeaveModel.id}}
                                            options={pilihanName}
                                            placeholder="-Pilih-"
                                            isDisabled={LeaveModel.x} />
                                            <span style={{ color: "red" }}>{errors["name"]}</span>
                                        <br />
                                        {/* <input type="text" id='leave_name'
                                            className="form-control" value={LeaveModel.leave_name}
                                            onChange={changeHandler("leave_name")}
                                        // disabled={LokerModel.x}
                                        /> */}

                                        <label className='form-label'> Berakhir pada(Akhir)</label>
                                        <input type="date"
                                            className='form-control' id='end'
                                            value={LeaveModel.end}
                                            onChange={changeHandler("end")}
                                        />
                                        <span style={{ color: "red" }}>{errors["end"]}</span>
                                        <br />

                                        <label className='form-label'> Kontak Selama Cuti </label>
                                        <br />
                                        <input type="text" id='leave_contact'
                                            className="form-control" value={LeaveModel.leave_contact}
                                            onChange={changeHandler("leave_contact")}
                                        />
                                        <span style={{ color: "red" }}>{errors["leave_contact"]}</span>
                                        <br />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-20">
                                <label className='form-label'> Alamat Selama Cuti </label>

                                <br />
                                <textarea class="form-control" rows="3" id='leave_address'
                                    className="form-control" value={LeaveModel.leave_address}
                                    placeholder="Enter ..."
                                    onChange={changeHandler("leave_address")}
                                />
                                <span style={{ color: "red" }}>{errors["leave_address"]}</span>
                                <br />
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div class="btn-group">
                            <button type="button" className="btn bg-orange" onClick={cancelHandler}  >      Close      </button>
                            <button type="button" className="btn bg-navy" onClick={saveLeave}       >      Save Change     </button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }

}
export default FormInput