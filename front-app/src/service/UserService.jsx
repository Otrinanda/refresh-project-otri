import axios from 'axios';
import {config} from '../config/config';
 const UserService ={
    getUser :()=>{
        const result = axios.get(config.apiUrl+'/getuser')
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    },
    postUser: (item) => {
        const result = axios.post(config.apiUrl + '/postuser', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    getUserbyid: (id) => {
        const result = axios.get(config.apiUrl + '/getuserbyid/'+id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    deleteUserbyid: (id) => {
        const result = axios.get(config.apiUrl + '/deleteuserbyid/'+id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    updateUser: (item) => {
        const result = axios.put(config.apiUrl + '/updateuser/'+item.Id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    deleteUser: (item) => {
        const result = axios.put(config.apiUrl + '/deleteuser/'+item.Id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    }
    
}
export default UserService;