module.exports = exports = (server, pool) => {

    //save/post user
    server.post('/api/postuser', (req, res) => {
        console.log(req.body)

        const { Nama, Email, Password, Active } = req.body;

        pool.query(`INSERT INTO public."User"(
             "Nama", "Email", "Password", "Active")
            VALUES ( '${Nama}','${Email}','${Password}','${Active}');`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Nama} berhasil disimpan`
                    })
                }
            }
        )
    });
    //untuk menampilkan databaser user
    server.get('/api/getuser', (req, res) => {

        pool.query(`SELECT * FROM  public."User" where "Active"=true `, 
        (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
    });
    //untuk show data user by id
    server.get('/api/getuserbyid/:id', (req, res) => {
        const id = req.params.id;
        pool.query(`SELECT * FROM  public."User" 
                    where "Id"=${id}`
        , (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                    
                })
            }
        }
        )
    });
    //untuk update / edit data user
    server.put('/api/updateuser/:id', (req, res) => {
        const id = req.params.id;
        const {Nama, Email, Password, Active } = req.body;
        console.log(`UPDATE public."User"
   
            SET     "Nama" = '${Nama}',
                    "Email" = '${Email}',
                    "Password" = '${Password}',
                    "Active" = '${Active}'
                    WHERE "Id"  =  ${id};`)
        pool.query(`UPDATE public."User"
   
        SET     "Nama" = '${Nama}',
                "Email" = '${Email}',
                "Password" = '${Password}',
                "Active" = '${Active}'
                WHERE "Id"  =  ${id};`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Nama} berhasil diubah`
                    });
                }
            }
        )
    });
    //untuk hapus data user
    server.put('/api/deleteuser/:id', (req, res) => {
        const id = req.params.id;
        const {Nama} = req.body;
        
        pool.query(`UPDATE public."User"
   
            SET     "Active" = 'false'
                    WHERE "Id"  =  '${id}';`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Nama} berhasil dihapus`
                    });
                }
            }
        )
    });
}