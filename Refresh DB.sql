PGDMP     :            	        x            Refresh    12.2    12.2 '    ?           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            @           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            A           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            B           1262    98525    Refresh    DATABASE     �   CREATE DATABASE "Refresh" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE "Refresh";
                postgres    false            �            1259    98540    Fakultas    TABLE     x   CREATE TABLE public."Fakultas" (
    "FakultasId" bigint NOT NULL,
    "FakultasNama" character varying(30) NOT NULL
);
    DROP TABLE public."Fakultas";
       public         heap    postgres    false            �            1259    98545    Fakultas_FakultasId_seq    SEQUENCE     �   ALTER TABLE public."Fakultas" ALTER COLUMN "FakultasId" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Fakultas_FakultasId_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99
    CACHE 1
);
            public          postgres    false    206            �            1259    98547    Gejala    TABLE     s   CREATE TABLE public."Gejala" (
    "GejalaId" bigint NOT NULL,
    "GejalaNama" character varying(200) NOT NULL
);
    DROP TABLE public."Gejala";
       public         heap    postgres    false            �            1259    98552    Gejala_GejalaId_seq    SEQUENCE     �   ALTER TABLE public."Gejala" ALTER COLUMN "GejalaId" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Gejala_GejalaId_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99
    CACHE 1
);
            public          postgres    false    208            �            1259    98554    Paparan    TABLE     v   CREATE TABLE public."Paparan" (
    "PaparanId" bigint NOT NULL,
    "PaparanNama" character varying(200) NOT NULL
);
    DROP TABLE public."Paparan";
       public         heap    postgres    false            �            1259    98559    Paparan_PaparanId_seq    SEQUENCE     �   ALTER TABLE public."Paparan" ALTER COLUMN "PaparanId" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Paparan_PaparanId_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99
    CACHE 1
);
            public          postgres    false    210            �            1259    98561 
   Perjalanan    TABLE        CREATE TABLE public."Perjalanan" (
    "PerjalananId" bigint NOT NULL,
    "PerjalananNama" character varying(100) NOT NULL
);
     DROP TABLE public."Perjalanan";
       public         heap    postgres    false            �            1259    98573    Perjalanan_PerjalananId_seq    SEQUENCE     �   ALTER TABLE public."Perjalanan" ALTER COLUMN "PerjalananId" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Perjalanan_PerjalananId_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99
    CACHE 1
);
            public          postgres    false    212            �            1259    98526 	   Responden    TABLE     �  CREATE TABLE public."Responden" (
    "Id" bigint NOT NULL,
    "Nama" character varying(50) NOT NULL,
    "Umur" character varying(5) NOT NULL,
    "Email" character varying(30) NOT NULL,
    "NoHp" character varying(15) NOT NULL,
    "Status" bigint,
    "Kelurahan" character varying(30) NOT NULL,
    "Kecamatan" character varying(30) NOT NULL,
    "Kabupaten" character varying(30) NOT NULL,
    "Fakultas" bigint,
    "Prodi" character varying(30) NOT NULL,
    "Gejala" bigint,
    "Perjalanan" character varying(50),
    "Paparan" bigint,
    "Transportasi" character varying(50),
    "Active" boolean NOT NULL,
    "CreatedBy" character varying(10) NOT NULL,
    "CreatedDate" timestamp without time zone NOT NULL,
    "ModifiedBy" character varying(10),
    "ModifiedDate" timestamp without time zone,
    "DeletedBy" character varying(10),
    "DeletedDate" timestamp without time zone
);
    DROP TABLE public."Responden";
       public         heap    postgres    false            �            1259    98531    Responden_Id_seq    SEQUENCE     �   ALTER TABLE public."Responden" ALTER COLUMN "Id" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Responden_Id_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99
    CACHE 1
);
            public          postgres    false    202            �            1259    98533    Status    TABLE     r   CREATE TABLE public."Status" (
    "StatusId" bigint NOT NULL,
    "StatusNama" character varying(30) NOT NULL
);
    DROP TABLE public."Status";
       public         heap    postgres    false            �            1259    98538    Status_StatusId_seq    SEQUENCE     �   ALTER TABLE public."Status" ALTER COLUMN "StatusId" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Status_StatusId_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99
    CACHE 1
);
            public          postgres    false    204            �            1259    98566    Transportasi    TABLE     �   CREATE TABLE public."Transportasi" (
    "TransportasiId" bigint NOT NULL,
    "TransportasiNama" character varying(50) NOT NULL
);
 "   DROP TABLE public."Transportasi";
       public         heap    postgres    false            �            1259    98571    Transportasi_TransportasiId_seq    SEQUENCE     �   ALTER TABLE public."Transportasi" ALTER COLUMN "TransportasiId" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Transportasi_TransportasiId_seq"
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99
    CACHE 1
);
            public          postgres    false    213            3          0    98540    Fakultas 
   TABLE DATA           B   COPY public."Fakultas" ("FakultasId", "FakultasNama") FROM stdin;
    public          postgres    false    206   �-       5          0    98547    Gejala 
   TABLE DATA           <   COPY public."Gejala" ("GejalaId", "GejalaNama") FROM stdin;
    public          postgres    false    208   �.       7          0    98554    Paparan 
   TABLE DATA           ?   COPY public."Paparan" ("PaparanId", "PaparanNama") FROM stdin;
    public          postgres    false    210   @/       9          0    98561 
   Perjalanan 
   TABLE DATA           H   COPY public."Perjalanan" ("PerjalananId", "PerjalananNama") FROM stdin;
    public          postgres    false    212   L0       /          0    98526 	   Responden 
   TABLE DATA           %  COPY public."Responden" ("Id", "Nama", "Umur", "Email", "NoHp", "Status", "Kelurahan", "Kecamatan", "Kabupaten", "Fakultas", "Prodi", "Gejala", "Perjalanan", "Paparan", "Transportasi", "Active", "CreatedBy", "CreatedDate", "ModifiedBy", "ModifiedDate", "DeletedBy", "DeletedDate") FROM stdin;
    public          postgres    false    202   �0       1          0    98533    Status 
   TABLE DATA           <   COPY public."Status" ("StatusId", "StatusNama") FROM stdin;
    public          postgres    false    204   2       :          0    98566    Transportasi 
   TABLE DATA           N   COPY public."Transportasi" ("TransportasiId", "TransportasiNama") FROM stdin;
    public          postgres    false    213   }2       C           0    0    Fakultas_FakultasId_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public."Fakultas_FakultasId_seq"', 12, true);
          public          postgres    false    207            D           0    0    Gejala_GejalaId_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."Gejala_GejalaId_seq"', 3, true);
          public          postgres    false    209            E           0    0    Paparan_PaparanId_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."Paparan_PaparanId_seq"', 3, true);
          public          postgres    false    211            F           0    0    Perjalanan_PerjalananId_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public."Perjalanan_PerjalananId_seq"', 10, true);
          public          postgres    false    215            G           0    0    Responden_Id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public."Responden_Id_seq"', 2, true);
          public          postgres    false    203            H           0    0    Status_StatusId_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."Status_StatusId_seq"', 4, true);
          public          postgres    false    205            I           0    0    Transportasi_TransportasiId_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public."Transportasi_TransportasiId_seq"', 5, true);
          public          postgres    false    214            �
           2606    98544    Fakultas Fakultas_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public."Fakultas"
    ADD CONSTRAINT "Fakultas_pkey" PRIMARY KEY ("FakultasId");
 D   ALTER TABLE ONLY public."Fakultas" DROP CONSTRAINT "Fakultas_pkey";
       public            postgres    false    206            �
           2606    98551    Gejala Gejala_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public."Gejala"
    ADD CONSTRAINT "Gejala_pkey" PRIMARY KEY ("GejalaId");
 @   ALTER TABLE ONLY public."Gejala" DROP CONSTRAINT "Gejala_pkey";
       public            postgres    false    208            �
           2606    98558    Paparan Paparan_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public."Paparan"
    ADD CONSTRAINT "Paparan_pkey" PRIMARY KEY ("PaparanId");
 B   ALTER TABLE ONLY public."Paparan" DROP CONSTRAINT "Paparan_pkey";
       public            postgres    false    210            �
           2606    98565    Perjalanan Perjalanan_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public."Perjalanan"
    ADD CONSTRAINT "Perjalanan_pkey" PRIMARY KEY ("PerjalananId");
 H   ALTER TABLE ONLY public."Perjalanan" DROP CONSTRAINT "Perjalanan_pkey";
       public            postgres    false    212            �
           2606    98537    Status Status_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public."Status"
    ADD CONSTRAINT "Status_pkey" PRIMARY KEY ("StatusId");
 @   ALTER TABLE ONLY public."Status" DROP CONSTRAINT "Status_pkey";
       public            postgres    false    204            �
           2606    98570    Transportasi Transportasi_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public."Transportasi"
    ADD CONSTRAINT "Transportasi_pkey" PRIMARY KEY ("TransportasiId");
 L   ALTER TABLE ONLY public."Transportasi" DROP CONSTRAINT "Transportasi_pkey";
       public            postgres    false    213            �
           2606    98530    Responden surveyor_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public."Responden"
    ADD CONSTRAINT surveyor_pkey PRIMARY KEY ("Id");
 C   ALTER TABLE ONLY public."Responden" DROP CONSTRAINT surveyor_pkey;
       public            postgres    false    202            3   �   x�%��
�0E�3_�P��^Z,E���`C�&�4��i]�p�=�c�D�)�����cCY�T�9pƍ��M���[�WF�A�Ǣ�13��6������>:I~��uA�ǅ_��l�o9��֚3�
L�C�Q�/ʔ>��[���W�z�2Ct����y*s�s��?v�D�      5   }   x��1�0k�+�� /@�4iiV�)>��X�#���Ь������dd�����8 ��є�4X�F�{���&1_��eYֶ
��,�ߨ�����aLy`�T�$��7�ҺrPF��BD?Ֆ0�      7   �   x����N�0���)<��_��0pYHL,�R7MӦ��ݷ�m3Kd�����U��pD"?�O,Z7,͈�,%U�NF�$$d�9���.��d�U��yz<��L]=�g��G����`�J\B$ϑ;��K�y���5��g��;%���v�Zz�?�8h@R�����7�A�i�����K�A��LC�P`�R6,�-q&Y��i�2��M�\��߹>M�����"�|Ҕ�o4Ƙo�/�I      9   �   x�-��
1���S�	d���'A֋�%���v�R[d��^�c>��G.����ej�q��o��F�Żb�;�k�=\YxR<�M�+�0jR<�'��\��`䅩��#�n)�@%|�Vʮ�8�+�ƅ��S��[nF�2-vL�7C��
��F>R      /     x�u��j�0����Dh��S܆Ĵ��\T�!�2�r��WnM���0�����F7%`�å���5V
���-��Ի�G����o�yX�?��ec���mJmS��r�uI�a�0�RP����g���`�O�AH�yB
�y������󙝍יbj�,x�Z�b�!��\?��:6�2T!���zM!�Fą�,�j�3�O\蔬d�v��gpx����/�ם�0�+�
�W�I�������;�[+�+Ψ(�oI�t_      1   X   x��;�  й=�.��pu�ѥ�L5�'���Z��,��$>Y�I�؃ݙT4��Ϧ����;���3mT���k��?=Za      :   b   x��1�@�:�)�qD� �Z�P�|�',$̲�__���N>���{�����_I���p��y���sT�ed>��5|5O�l1٪�<u�����u{�     