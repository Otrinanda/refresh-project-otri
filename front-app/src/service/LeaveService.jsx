import axios from 'axios';
import {config} from '../config/config';
 const LeaveService ={
    getAll :(filter)=>{
        const result = axios.post(config.apiUrl+'/getleave',filter)
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    },

    getType: () => {
        const result = axios.get(config.apiUrl + '/gettype')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    getName: () => {
        const result = axios.get(config.apiUrl + '/getname')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    postLeave: (item) => {
        const result = axios.post(config.apiUrl + '/leavepost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getleavebyid: (id) => {
        const result = axios.get(config.apiUrl + '/getleavebyid/'+id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    delete: (item) => {
        const result = axios.put(config.apiUrl + '/deleteleave/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateLeave: (item) => {
        const result = axios.put(config.apiUrl + '/updateLeave/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    countleave:(filter) => {
        const result = axios.post(config.apiUrl+'/countleave', filter)
        .then(response =>{
            return{
                success: response.data.success, 
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },

    getQuota: () => {
        const result = axios.get(config.apiUrl + '/getquota')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result
    },
    getTaken: () => {
        const result = axios.get(config.apiUrl + '/gettaken')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result
    },

    postTaken :( JumlahTaken,ListQuota)=>{
        const result = axios.post(config.apiUrl+'/posttaken', JumlahTaken,ListQuota)
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    },

    cekDate: (item) => {
        const result = axios.post(config.apiUrl + '/cekdate',item)
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result
    },
}
export default LeaveService