module.exports=exports =(server,pool)=>{

    server.post('/api/gettimesheet',(req,res)=>{

        const { tahun,bulan } = req.body; 
        let qFilterTahun=
        ( tahun !=="0")? `and extract(year from timesheet_date)='${tahun}'`:
        `and extract(year from timesheet_date)='0' `

        let qFilterBulan=
        ( bulan !=="0")? `and extract(month from timesheet_date)='${bulan}'`:
        `and extract(month from timesheet_date)='0' `
        
        var query=`select id, 
        status ,TO_CHAR("timesheet_date" :: DATE, 'dd Monthyyyy') as timesheet_date,
         start,
         "end",
         overtime,start_ot,end_ot ,placement_id,activity 
        from public.x_timesheet
        where user_approval ='Submitted' 
        and ero_status is null 
        and is_delete ='false' 
        ${qFilterTahun} ${qFilterBulan}
        order by timesheet_date`
        pool.query(query,
        (error,result)=>{
            if (error){
                res.send(400,{
                    success:false,
                    result: error
                })
            }
            else {
                res.send(200,{
                    success:true,
                    result:result.rows
                })
            }
        })
        console.log(query)
    }),

    server.get('/api/getdetail/:id',(req,res)=>{
        const id=req.params.id;
        console.log( `select id, status ,TO_CHAR("timesheet_date" :: DATE, 'dd Monthyyyy') as timesheet_date, start,"end",overtime,start_ot,end_ot ,placement_id,activity from public.x_timesheet
        where user_approval ='Submitted' and ero_status is null and is_delete ='false' and extract(year from timesheet_date)='2020'
        and id=${id}` )

        pool.query(`select id, status ,TO_CHAR("timesheet_date" :: DATE, 'dd Monthyyyy') as timesheet_date, start,"end",overtime,start_ot,end_ot ,placement_id,activity from public.x_timesheet
        where user_approval ='Submitted' and ero_status is null and is_delete ='false' and extract(year from timesheet_date)='2020'
        and id=${id}`
        ,(error,result)=>{
            if(error){
                res.send(400,{
                    success:false,
                    result:error
                })
            }
            else{
                res.send(200,{
                    success:true,
                    result:result.rows[0]
                })
            }
        })
    }),

    server.get('/api/gettahun', (req, res) => {
        var query = `select * from public."ListTahun";`;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
    });

    server.get('/api/getbulan', (req, res) => {
        var query = `SELECT * FROM public."ListBulan"`;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
    });

    server.post('/api/submit',(req,res) =>{
        let detQuery = '';
        const{ListApprove,nilai,hasil,kompetensi,disiplin,lolos
                }=req.body;
        
                 console.log(ListApprove)
                 console.log(nilai)
                 console.log(lolos)
                // console.log(hasil)
                // console.log(kompetensi)
                // console.log(disiplin)
       // ListApprove.forEach(data=>{
         //   detQuery +=detQuery.length >0? `,`: ``;
             detQuery=`(
                 '${lolos.id}',
                   current_timestamp,
                 'false',
                 extract(year from timestamp '${lolos.timesheet_date}'),
                 extract(month from timestamp '${lolos.timesheet_date}'), 
                 '${lolos.placement_id}','${nilai.hasil}','${nilai.kompetensi}','${nilai.disiplin}')`
        // });

        var query=`INSERT INTO public.x_timesheet_assessment(
             created_by, created_on, is_delete, year, month, placement_id, target_result, competency, discipline)
             VALUES ${detQuery}
             ;`
             console.log(query)
             pool.query(query, (error, result) => {
                if (error) {
                     res.send(400, {
                         success: false,
                         result: {
                             detail: error.detail
                         }
                     })
                 } else {
                     res.send(201, {
                         success: true,
                         result: "Terimakasih sudah Menilai ! :) "
                     })
                 }
             });

    })

    server.post('/api/tolak',(req,res) =>{
        let detQuery = '';
        const{ListApprove
                }=req.body;
        
               // console.log(ListApprove)
 
        ListApprove.forEach(data=>{
            detQuery +=detQuery.length >0? `,`: ``;
             detQuery+=`'${data.id}'`
         });

        var query=`UPDATE public.x_timesheet
        SET  user_approval= 'Rejected'
        WHERE "id" in (${detQuery})
             ;`
             console.log(query)
             pool.query(query, (error, result) => {
                if (error) {
                     res.send(400, {
                         success: false,
                         result: {
                             detail: error.detail
                         }
                     })
                 } else {
                     res.send(201, {
                         success: true,
                         result: "Data sudah sudah ditolak ! :) "
                     })
                 }
             });

    })

    server.post('/api/onapprove',(req,res) =>{
        let detQuery = '';
        const{ListApprove
                }=req.body;
        
        //        console.log(ListApprove)
 
        ListApprove.forEach(data=>{
            detQuery +=detQuery.length >0? `,`: ``;
             detQuery+=`'${data.id}'`
         });
         
        // WHERE "id" in ('1' , '2')
        var query=`UPDATE public.x_timesheet
        SET  user_approval= 'Approved',
        approved_on=current_timestamp
        WHERE "id" in (${detQuery})
             ;`
             console.log(query)
             pool.query(query, (error, result) => {
                if (error) {
                     res.send(400, {
                         success: false,
                         result: {
                             detail: error.detail
                         }
                     })
                 } else {
                     res.send(201, {
                         success: true,
                         result: "Data sudah diApprove ! :) "
                     })
                 }
             });

    })

    server.get('/api/getnilai', (req, res) => {
        var query = `select * from public."DatabaseNilai"
        order by "id" desc`;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
        //console.log(query);
    });

    server.post('/api/cektimesheet', (req, res) => {

        const { tahun,bulan } = req.body; 
        let qFilterTahun=
        ( tahun !=="0")? `and extract(year from timesheet_date)='${tahun}'`:
        `and extract(year from timesheet_date)='0' `

        let qFilterBulan=
        ( bulan !=="0")? `and extract(month from timesheet_date)='${bulan}'`:
        `and extract(month from timesheet_date)='0' `
        
        var query=`select id 
        from public.x_timesheet
        where user_approval ='Submitted' 
        and ero_status is null 
        and is_delete ='false' 
        ${qFilterTahun} ${qFilterBulan}`

        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]
                    })
                }
            }
        )
       // console.log(query)
    });

}