import axios from 'axios';
import {config} from '../config/config';
 const OrderService ={
    getAll :()=>{
        const result = axios.get(config.apiUrl+'/getbarang')
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    }, 
    post: (ListOrder) => {
        const result = axios.post(config.apiUrl + '/orderpost', ListOrder)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    getAlltransaksi:() => {
        const result = axios.get(config.apiUrl+'/getAlltransaksi')
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },
    getAllbarangorder:() => {
        const result = axios.get(config.apiUrl+'/getAllbarangorder')
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },
}
export default OrderService;