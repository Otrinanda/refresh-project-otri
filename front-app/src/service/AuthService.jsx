import axios from 'axios';
import {config} from '../config/config';

 export const AuthService ={
    
    postLogin: (item) => {
        const result = axios.post(config.apiUrl + '/login', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    }   
}
export default AuthService;