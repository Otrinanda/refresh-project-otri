import axios from 'axios';
import { config } from '../config/config';

const LokerService = {
    getSumber: () => {
        const result = axios.get(config.apiUrl + '/getsumber')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },
    getTipe: () => {
        const result = axios.get(config.apiUrl + '/gettipe')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    postLoker: (item) => {
        const result = axios.post(config.apiUrl + '/lokerpost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getBiodata: () => {
        const result = axios.get(config.apiUrl + '/getbiodata')
            .then(response => {
                return {
                    success: response.data.success,
                    result: response.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result
    },

    postBiodata: (item) => {
        const result = axios.post(config.apiUrl + '/biodatapost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdetailbyid: (identity_no) => {
        const result = axios.get(config.apiUrl + '/getdetailbyid/' + identity_no)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    createLoker: (item) => {
        const result = axios.put(config.apiUrl + '/createloker/' + item.identity_no, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateLoker: (item) => {
        const result = axios.put(config.apiUrl + '/updateloker/' + item.identity_no, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    getId: (identity_no) => {
        const result = axios.get(config.apiUrl + '/getid/' + identity_no)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    postBiodataId: (identity_no) => {
        const result = axios.post(config.apiUrl + '/biodataidpost/'+identity_no)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
}

export default LokerService;