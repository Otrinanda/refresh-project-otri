import React from 'react';
import Table from 'react-bootstrap/Table';
import 'bootstrap/dist/css/bootstrap.min.css';
import BarangService from '../service/BarangService';
import OrderService from '../service/OrderService';
import Button from 'react-bootstrap/Button';

import 'bootstrap/dist/css/bootstrap.min.css';
import FormInputOrder from './formbelanja';
import {config} from '../config/config';


class Order extends React.Component {

    BarangModel = {
        id: 0,
        Nama_barang: '',
        Qty: 0,
        Harga: 0,
        Kategori_id: "",
        Kategori_nama: ""
    }

    constructor() {
        super();
        this.state = {
            ListBarang: [],
            ListOrder :[],
            ListTransaksi:[],
            ListTampil:[],
            BarangModel: this.BarangModel,
            hidden: true,
            mode: "",
            openmodal: false,
            openmodalEdit: false,
            modalqty:false,
            barang:{}
        }
    }

    componentDidMount() {
        this.loadList();
        this.loadlistBarangOrder();
    }


    loadList = async () => {
        const respon = await OrderService.getAlltransaksi();
        if (respon.success) {
            this.setState({
                ListTransaksi: respon.result
            })
        }
    }
    loadListBarang = async () => {
        const respon = await BarangService.getAll();
        if (respon.success) {
            this.setState({
                ListBarang: respon.result
            })
        }
    }
    loadlistBarangOrder = async () => {
        const respon = await OrderService.getAllbarangorder();
        if (respon.success) {
            this.setState({
                ListTampil: respon.result
            })
        }
    }
    handleTampil = () => {
        this.loadListBarang();
        this.setState({
            BarangModel: this.BarangModel,
            openmodalEdit: true,
            mode: "create",
        })
    }
    onInputQty = name=> ({target:{value}})=>{
      //alert(name +value)
            this.setState({
                barang:{
                    ...this.state.barang,
                    [name] : value
                }
            })
    }
    onSelect = (brg) =>{
        alert(JSON.stringify(brg))
        const{ListOrder} = this.state;
        let idx = ListOrder.findIndex(i => i.id === brg.id);

        if (idx !== -1){
            ListOrder.splice(idx,1);
        }else{
            this.setState({
                modalqty :true,
                barang : {
                id : brg.id,
                Harga : brg.Harga,
                Qty : brg.Qty,
                id_Customer : localStorage.getItem(config.id)
            }})
                    }
        //alert(JSON.stringify(ListOrder))
    }

    onSetQty=()=>{
        const {ListOrder,barang} = this.state;
        ListOrder.push(barang);
        this.setState({
            modalqty:false
        })
        }

    onSave = async () => {
        const ListOrder = {
            ListOrder: this.state.ListOrder
        }
        //alert(JSON.stringify(ListOrder))
        const respons = await OrderService.post(ListOrder);
        if (respons.success) {
            alert('Success : ' + respons.result)
        } else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalEdit: false
        })
        this.componentDidMount();
        this.setState({
            ListOrder: []
        })
    }
    cancelHandler=async()=>{
        this.setState({
            openmodal:false
        })
    }

    

    



    render() {
        const {ListBarang, ListTransaksi,ListTampil,
             hidden, 
             mode, openmodalEdit,modalqty,barang} 
             = this.state;
        return (
            <div>
                <h5> List Order</h5>
                <FormInputOrder 
                    ListBarang={ListBarang}
                    handleTampil={this.handleTampil}
                    hidden={hidden}
                    mode={mode}
                    openmodalEdit={openmodalEdit}
                    cancelHandler={this.cancelHandler}
                    onSave={this.onSave}
                    onSelect={this.onSelect}
                    modalqty={modalqty}
                    barang={barang}
                    onInputQty = {this.onInputQty}
                    onSetQty = {this.onSetQty}
                    />
            {
                    ListTransaksi.map(trx => {
                        return (
                            <div>
                                <h3>Kode Transaksi: {trx.Kode_Transaksi} </h3>
                                <Table stripped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Beli</th>
                                            <th>Harga Satuan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            ListTampil.map(brg => {
                                                if (trx.Kode_Transaksi === brg.Kode_Transaksi)
                                                    return (
                                                        <tr>
                                                            <td>{brg.Kd_Barang}</td>
                                                            <td>{brg.Nama_Barang}</td>
                                                            <td>{brg.Quantity}</td>
                                                            <td>{brg.Harga}</td>
                                                        </tr>
                                                    )
                                            })
                                        }
                                    </tbody>
                                </Table>
                                <div className="col md-2 offset-md-10">
                                    <p><b> Total Barang: {trx.totalquantity}</b></p>
                                    <p><b> Total Harga: {trx.totalharga}</b> </p>
                                    <Button variant="danger" > Delete </Button>&emsp;
                                    <Button variant="success" > Checkout </Button>
                                </div>  
                            </div>
                        )
                    })
                }
                
            </div>
        )
    }
}

export default Order;
