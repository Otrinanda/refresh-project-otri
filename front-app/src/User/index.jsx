import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css'
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import UserService from '../service/UserService';
import FormInputUser from './forminputuser';
import  CheckBox  from './CheckBox'

class User extends React.Component {
    UserModel = {
        Id: 0,
        Nama: "",
        Email: "",
        Password: '',
        Active: false
    }
    constructor() {
        super();
        this.state = {
            ListUser: [],
            UserModel: this.UserModel,
            hidden: true,
            mode: "",
            openmodal: false,
            openmodaldel: false,
            fields: {},
            errors: {},
            fruites: [
                { id: 1, value: "banana", isChecked: false },
                { id: 2, value: "apple", isChecked: false },
                { id: 3, value: "mango", isChecked: false },
                { id: 4, value: "grap", isChecked: false }
            ]
        }
    }


    handleValidation() {
        let fields = this.state.UserModel;
        let errors = {};
        let formIsValid = true;

        //Name
        if (!fields["Nama"]) {
            formIsValid = false;
            errors["Nama"] = "Cannot be empty";
        }

        if (typeof fields["Nama"] !== "undefined") {
            if (!fields["Nama"].match(/^[a-zA-Z]+$/)) {
                formIsValid = false;
                errors["Nama"] = "Only letters";
            }
        }

        //Email
        if (!fields["Email"]) {
            formIsValid = false;
            errors["Email"] = "Cannot be empty";
        }

        if (typeof fields["Email"] !== "undefined") {
            let lastAtPos = fields["Email"].lastIndexOf('@');
            let lastDotPos = fields["Email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["Email"].indexOf('@@') === -1 && lastDotPos > 2 &&
                (fields["Email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["Email"] = "Email is not valid";
            }
        }

        this.setState({
            errors: errors
        });
        console.log(errors)
        return formIsValid;
    }

    componentDidMount() {
        this.loadList();
    }

    loadList = async () => {
        const respon = await UserService.getUser();

        if (respon.success) {
            this.setState({
                ListUser: respon.result
            })
        }

    }
    cancelHandler = async () => {
        const respon = await UserService.getUser();

        if (respon.success) {
            this.setState({
                ListUser: respon.result,
                openmodal: false
            })
        }

    }
    handleTampil = () => {
        this.setState({
            hidden: true,
            openmodal: true,
            UserModel: this.UserModel,
            mode: "create"
        })
    }

    handleEdit = async (id) => {

        const respon = await UserService.getUserbyid(id);
        console.log(respon)

        if (respon.success) {
            this.setState({
                hidden: false,
                openmodal: true,
                UserModel: respon.result,
                mode: "edit"
            })
        }
        else {
            alert('Error:' + respon.result)
        }
    }
    handleDelete = async (id) => {
        const respon = await UserService.getUserbyid(id);
        console.log(respon)
        if (respon.success) {
            this.setState({

                openmodaldel: true,
                UserModel: respon.result,

            })
        }
        else {
            alert('Error:' + respon.result)
        }
    }

    changeHendler = name => ({ target: { value } }) => {
        this.setState({
            UserModel: {
                ...this.state.UserModel,
                [name]: value
            }
        })
    }

    checkedHendler = name => ({ target: { checked } }) => {

        this.setState({
            UserModel: {
                ...this.state.UserModel,
                [name]: checked
            }
        })
    }
    sureDelete = async (item) => {
        const { UserModel } = this.state;
        const respons = await UserService.deleteUser(UserModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList();

        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodaldel: false
        })
    }
    onSave = async () => {
        const { UserModel, mode } = this.state;
        if (this.handleValidation()) {
            if (mode === "create") {
                const respons = await UserService.postUser(UserModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadList();


                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodal: false
                })
            }
            else {
                //fungsi edit
                const respons = await UserService.updateUser(UserModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadList();

                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodal: false
                })
            }
        } else {
            alert("Form has errors.")
        }


    }

    onSelect = (user) => {
        alert(JSON.stringify(user))
        //     const { ListApprove, lolos } = this.state;
        //     let idx = ListApprove.findIndex(i => i.id === tmsht.id)
        //     if (idx !== -1) {
        //         ListApprove.splice(idx, 1);
        //     }
        //     else {
        //         this.setState({
        //             openmodalcoba: true,
        //             lolos: {
        //                 id: tmsht.id,
        //                 status: tmsht.status,
        //                 timesheet_date: tmsht.timesheet_date,
        //                 placement_id: tmsht.placement_id,
        //                 activity: tmsht.activity,
        //                 start: tmsht.start,
        //                 end: tmsht.end,
        //                 overtime: tmsht.overtime,
        //                 start_ot: tmsht.start_ot,
        //                 end_ot: tmsht.end_ot
        //             }
        //         })
        //     }
        //     //this.onSetCheck();
    }

    

    handleAllChecked = (event) => {
        let fruites = this.state.fruites
        fruites.forEach(fruite => fruite.isChecked = event.target.checked)
        this.setState({ fruites: fruites })
    }

    handleCheckChieldElement = (event) => {
        let fruites = this.state.fruites
        fruites.forEach(fruite => {
            if (fruite.value === event.target.value)
                fruite.isChecked = event.target.checked
        })
        this.setState({ fruites: fruites })
    }


    render() {
        const { ListUser,
            UserModel,
            hidden,
            KategoriOption,
            mode,
            openmodal,
            openmodaldel,
            cancelHandler,
            errors
        } = this.state;
        return (
            <div>
                <h5> List User </h5>
                <h1> Check and Uncheck All Example </h1>
                <input type="checkbox" onClick={this.handleAllChecked} value="checkedall" /> Check / Uncheck All
                <ul>
                    {
                        this.state.fruites.map((fruite) => {
                            return (<CheckBox handleCheckChieldElement={this.handleCheckChieldElement}  {...fruite} />)
                        })
                    }
                </ul>
                <FormInputUser
                    handleTampil={this.handleTampil}
                    changeHendler={this.changeHendler}
                    checkedHendler={this.checkedHendler}
                    UserModel={UserModel}
                    onSave={this.onSave}
                    hidden={hidden}
                    title={mode}
                    KategoriOption={KategoriOption}
                    selectedHandler={this.selectedHandler}
                    mode={mode}
                    openmodal={openmodal}
                    openmodaldel={openmodaldel}
                    cancelHandler={cancelHandler}
                    errors={errors}
                />

                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <td>check</td>
                            <th> Id </th>
                            <th> Nama </th>
                            <th> Email </th>
                            <th> Password </th>
                            <th> Active </th>
                            <th> Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        {
                            ListUser.map(user => {
                                return (
                                    <tr>
                                        <td>

                                            <Form.Group controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" size="lg"
                                                    label="Check me out" isChecked={false}
                                                    onClick={() => this.onSelect(user)}
                                                />
                                            </Form.Group>


                                        </td>
                                        <td>{user.Id}</td>
                                        <td>{user.Nama}</td>
                                        <td>{user.Email}</td>
                                        <td>{user.Password}</td>
                                        <td>{user.Active.toString()}</td>
                                        <td>
                                            <Button variant='warning' onClick={() => this.handleEdit(user.Id)}>Edit</Button> &emsp;
                                            <Button variant='danger' onClick={() => this.handleDelete(user.Id)}>Delete</Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }

                    </tbody>
                </Table>


                <div>
                    disini ya
                    <Form>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                    </Form>
                </div>

                <Modal show={openmodaldel}>
                    <Modal.Body> Anda Yakin Ingin Menghapus Data {UserModel.Nama} ? </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={() => this.sureDelete(UserModel.Id)}> Tentu! </Button>
                        <Button variant="primary" onClick={cancelHandler}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
export default User;