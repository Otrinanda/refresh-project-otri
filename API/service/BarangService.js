module.exports = exports = (server, pool) => {
    server.post('/api/barangpost', (req, res) => {
        console.log(req.body)
        const { Kd_Barang, Nama_Barang, Qty, Harga, Active ,Kategori_Id,Created_By,Created_Date} = req.body;
        pool.query(`INSERT INTO public."Barang"(
            "Kd_Barang", "Nama_Barang", "Qty", "Harga", "Active", "Kategori_Id", "Created_By", "Created_Date")
            VALUES ('${Kd_Barang}', '${Nama_Barang}', '${Qty}', '${Harga}', '${Active}', '${Kategori_Id}', '${Created_By}', current_timestamp);`,
            
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Nama_Barang} berhasil disimpan`
                    })
                }
            }
        )
    });

    server.get('/api/getbarang', (req, res) => {

        pool.query(`SELECT a.* , m."Nama" as Kategori_Nama 
        FROM  public."Barang" as a join public."Kategori" as m
        on a."Kategori_Id"= m."Id"
        order by "id"`, 
        (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
    });
    server.get('/api/getbarangbyid/:id', (req, res) => {
        const id = req.params.id;
        pool.query(`SELECT a.* , m."Nama" as Kategori_Nama
        FROM  public."Barang" as a join public."Kategori" as m
        on a."Kategori_Id"= m."Id"
                    where a."id"=${id}`
        , (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                    
                })
            }
        }
        )
    });
    server.put('/api/updatebarang/:id', (req, res) => {
        const id = req.params.id;
        const { Kd_Barang, Nama_Barang, Qty, Harga, Active,Kategori_Id,Created_By,Created_Date } = req.body;
        console.log(`UPDATE public."Barang"
   
            SET     "Kd_Barang" = '${Kd_Barang}', 
                    "Nama_Barang" = '${Nama_Barang}',
                    "Qty" = ${Qty},
                    "Harga" = ${Harga},
                    "Active" = '${Active}',
                    "Kategori_Id" ='${Kategori_Id}',
                    "Created_By"='${Created_By}',
                    "Created_Date'= DEFAULT
                    WHERE "id"  =  ${id}`)
        pool.query(`UPDATE public."Barang"
   
            SET     "Kd_Barang" = '${Kd_Barang}', 
                    "Nama_Barang" = '${Nama_Barang}',
                    "Qty" = ${Qty},
                    "Harga" = ${Harga},
                    "Active" = '${Active}',
                    "Kategori_Id"='${Kategori_Id}',
                    "Created_By"='${Created_By}',
                    "Created_Date'= DEFAULT
                    WHERE "id"  =  ${id};`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Kd_Barang} berhasil diubah`
                    });
                }
            }
        )
    });
    server.put('/api/deletebarang/:id', (req, res) => {
        const id = req.params.id;
        const { Kd_Barang } = req.body;
        
        pool.query(`UPDATE public."Barang"
   
            SET     "Active" = 'false'
                    WHERE "id"  =  ${id};`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Kd_Barang} berhasil dihapus`
                    });
                }
            }
        )
    });
}