import React from 'react';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';

import Modal from 'react-bootstrap/Modal';


import 'bootstrap/dist/css/bootstrap.min.css';
import ModalHeader from '../../node_modules/react-bootstrap/ModalHeader';


class FormInputOrder extends React.Component {
    render() {
        const {ListBarang, handleTampil, barang,onInputQty,onSetQty,
            mode, openmodalEdit, modalqty,onSelect,
            cancelHandler,onSave} 
            = this.props;
        return (
            <div>
                
                <Button variant="primary" onClick={handleTampil}>ORDER HERE!</Button>
                <Modal show={modalqty}style={{opacity:1, paddingTop:"10%",paddingLeft:"5%"}}>
                    <ModalHeader style ={{backgroundColor:"aqua"}}>
                        <Modal.Title> Jumlah Beli? </Modal.Title>
                    </ModalHeader>
                    <Modal.Body>
                        <label>Stok : {barang.Qty} </label> <br/>
                        <label>Harga : {barang.Harga} </label> <br/>
                        <input type="text" id="qtybeli" onChange={onInputQty("qtybeli")}/>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="warning"> Cancel </Button>
                        <Button variant="primary" onClick={onSetQty}> OK ! </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={openmodalEdit} style={{opacity:1}}>
                    <Modal.Header style={{ backgroundColor: "green" }}>
                        <Modal.Title>{mode}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                    <label className='form-label'> Kode Transaksi </label>
                    <input type="text" className='form-control' placeholder="Kode transaksi akan ter-generate otomatis " /><br/>
                    <Table stripped bordered hover>
                        <thead>
                            <tr>
                            <th>Pilih</th>
                            <th>Nama barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            ListBarang.map(brg => {
                                return (
                                    <tr>
                                        <td>
                                        <input type="checkbox" className='form-check-input'onClick={() => onSelect(brg)}/>
                                        </td>
                                        <td>{brg.Nama_Barang}</td>
                                        <td>{brg.Qty}</td>
                                        <td>{brg.Harga}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={cancelHandler}> Cancel </button> &emsp;
                            <button type="button" className="btn btn-primary"onClick={onSave}> Save </button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
export default FormInputOrder;
