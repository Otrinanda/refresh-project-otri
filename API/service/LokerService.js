module.exports = exports = (server, pool) => {


    server.get('/api/getsumber', (req, res) => {
        var query = `SELECT * FROM public."DatabaseSumber";`;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
        //console.log(query);
    });

    server.get('/api/gettipe', (req, res) => {
        var query = `SELECT * FROM public."DatabaseTipe";`;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        }
        )
        //console.log(query);
    });

    server.post('/api/lokerpost', (req, res) => {
        console.log(req.body)
        const {
            id,
            createdBy,
            createdOn,
            modifiedBy,
            modifiedOn,
            deleteBy,
            deleteOn,
            biodataId,
            vacancySource,
            candidateType,
            eventName,
            careerCenterName,
            referrerName,
            referrerPhone,
            referrerEmail,
            otherSource,
            last_income,
            applyDate,
            identity_no
        } = req.body;

        let penghasilan = lastIncome != "undefined" ? `'${lastIncome}'` : `''`;

        var query = `INSERT INTO public.x_sumber_loker(
            created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, vacancy_source, candidate_type, event_name, career_center_name, referrer_name, referrer_phone, referrer_email, other_source, last_income, apply_date)
           VALUES 
           ('${createdBy}', current_date, '123', current_date, null, null, false, '${identity_no}', '${vacancySource}', '${candidate_type}', '${eventName}', '${careerCenterName}', '${referrerName}','${referrerPhone}' ,'${referrerEmail}', '${otherSource}', '${penghasilan}', '${applyDate}');
       `
        pool.query(query,

            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${applyDate} berhasil disimpan`
                    })
                }
            }
        ); console.log(query)
    });

    server.get('/api/getbiodata', (req, res) => {

        pool.query(`select * from public.x_biodata`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        )
    });

    server.post('/api/biodatapost', (req, res) => {
        console.log(req.body)
        const {
            createdBy,
            fullname,
            identity_no
        } = req.body;
        pool.query(`INSERT INTO public.x_biodata(
            created_by, created_on,  is_delete, fullname,  identity_no)
            VALUES ( '${createdBy}', current_date, 'false', '${fullname}', '${identity_no}');
       `,

            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: `Data ${fullname} berhasil disimpan`
                    })
                }
            }
        )
    });

    server.get('/api/getdetailbyid/:identity_no', (req, res) => {
        const identity_no = req.params.identity_no;


        var query = `SELECT 
    m.fullname,m.identity_no,
    a.vacancy_source,a.candidate_type,
    a.event_name,a.career_center_name,
    a.referrer_name,a.referrer_phone,
    a.referrer_email,a.other_source,
    a.last_income,
    TO_CHAR(a."apply_date" :: DATE, 'yyyy-MM-dd') as "apply_date"

    FROM public.x_sumber_loker as a join public.x_biodata as m
    on a."biodata_id" = m."identity_no"

        where a."biodata_id"='${identity_no}';`
        pool.query(query
            , (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]

                    })
                }
            }
        )
        console.log(query)
    });

    server.put('/api/updateloker/:identity_no', (req, res) => {
        const identity_no = req.params.identity_no;
        const {
            vacancy_source,
            candidate_type,
            event_name,
            career_center_name,
            referrer_name,
            referrer_phone,
            referrer_email,
            other_source,
            last_income,
            apply_date,

        } = req.body;

        let qFilter =
            (vacancy_source === "Event") ? ` 
            "event_name"='${event_name}', 
            "career_center_name"='', 
            "referrer_name"='', 
            "referrer_phone"='', 
            "referrer_email"='', 
            "other_source"='',`
                :
                (vacancy_source === "Carrer Center") ? ` 
            "event_name"='', 
            "career_center_name"='${career_center_name}', 
            "referrer_name"='', 
            "referrer_phone"='', 
            "referrer_email"='', 
            "other_source"='',`
                    :
                    (vacancy_source === "Referensi") ? ` 
            "event_name"='', 
            "career_center_name"='', 
            "referrer_name"='${referrer_name}', 
            "referrer_phone"='${referrer_phone}', 
            "referrer_email"='${referrer_email}', 
            "other_source"='}',`
                        :
                        (vacancy_source === "Sumber Lainnya") ? ` 
            "event_name"='', 
            "career_center_name"='', 
            "referrer_name"='', 
            "referrer_phone"='', 
            "referrer_email"='', 
            "other_source"='${other_source}',`
                            :
                            ` `
                            let penghasilan = last_income != "undefined" ? `'${last_income}'` : ``; 
        var query = `UPDATE public.x_sumber_loker
                        SET  
                        "modified_by" = '${identity_no}',
                        "modified_on"= current_timestamp,
                        "vacancy_source"='${vacancy_source}', 
                        "candidate_type"='${candidate_type}', 
                        ${qFilter}  
                        "last_income"='${penghasilan}', 
                        "apply_date"='${apply_date}'

                    WHERE "biodata_id"='${identity_no}';`
        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });
            }
            else {
                res.send(200, {
                    success: true,
                    result: `Data ${identity_no} berhasil diubah`
                });
            }
        }
        )
    });

    server.put('/api/createloker/:identity_no', (req, res) => {
        const identity_no = req.params.identity_no;
        const {
            vacancy_source,
            candidate_type,
            event_name,
            career_center_name,
            referrer_name,
            referrer_phone,
            referrer_email,
            other_source,
            last_income,
            apply_date,

        } = req.body;

        let qFilter =
            (vacancy_source === "Event") ?
                ` '${event_name}', '','','','',''`
                :
            (vacancy_source === "Carrer Center") ? 
                `'', '${career_center_name}', '' , '' , '' , ''`
                :
            (vacancy_source === "Referensi") ? 
            ` '' , '' , '${referrer_name}','${referrer_phone}','${referrer_email}', '' `
            :
            (vacancy_source === "Sumber Lainnya") ? 
            ` '' , '' , '' , '' , '' ,  '${other_source}' `
            :
            ` `

        var query = `INSERT INTO public.x_sumber_loker(
            created_by, created_on, is_delete, 
            biodata_id, vacancy_source, candidate_type, 
            event_name, career_center_name, referrer_name, 
            referrer_phone, referrer_email, other_source, 
            last_income, apply_date)
            VALUES 
            ('${identity_no}',current_timestamp , 'false', 
            '${identity_no}', '${vacancy_source}', '${candidate_type}', 
            ${qFilter}  
            , '${last_income}', '${apply_date}');
            `
            
        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });
            }
            else {
                res.send(200, {
                    success: true,
                    result: `Data ${identity_no} berhasil diubah`
                });
            }
        }
        )
    });

    server.post('/api/biodataidpost/:identity_no', (req, res) => {

        console.log(req.body)
        const identity_no = req.params.identity_no
        // const {
        //     identity_no
        // } = req.body;
        console.log(identity_no)
        var query = `INSERT INTO public.x_sumber_loker(
            created_by, created_on, is_delete, biodata_id )
           VALUES 
           ('${identity_no}', current_timestamp, false, '${identity_no}');
       `
        pool.query(query,

            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]
                    })
                }
            }
        )
        console.log(query)
    });

    server.get('/api/getid/:identity_no', (req, res) => {
        const identity_no = req.params.identity_no;
        console.log(`select id
        from public.x_sumber_loker

        where "biodata_id"='${identity_no}';`)
        pool.query(`select id
        from public.x_sumber_loker
        
                where "biodata_id"='${identity_no}';`
            , (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                }
                else {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]

                    })
                }
            }
        )
    });
}
