import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';

import ModalHeader from '../../node_modules/react-bootstrap/ModalHeader';

class FormDetail extends React.Component {

    constructor() {
        super();
        this.state = {

        }
    }
    render() {
        const {
            openmodalDetail,
            openmodalcoba,
            openmodalcobaall,
            onSetCheck,
            onSetCheckAll,
            TimesheetModel,
            handleCancel
        } = this.props

        return (
            <div>
                <Modal show={openmodalDetail} style={{ opacity: 1 }} size="lg" >
                    <Modal.Header style={{ backgroundColor: "navy" }}>
                        <div class="text-white">Detail Timesheet </div>
                        <br />
                        <div class="text-white">ID = {TimesheetModel.id}</div>
                    </Modal.Header>
                    <Modal.Body>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h7 class="text-navy">Status = </h7> 
                                    <h5 class="text-navy">{TimesheetModel.status}</h5>

                                    <h7 class="text-navy">Tanggal = </h7>
                                    <h5 class="text-navy">{TimesheetModel.timesheet_date}</h5>
                                    
                                    <h7 class="text-navy">Overtime = </h7>
                                    <h5 class="text-navy">{TimesheetModel.overtime}</h5>
                                    

                                    <h7 class="text-navy">Kegiatan= </h7>
                                    <h5 class="text-navy">{TimesheetModel.activity}</h5>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <h7 class="text-navy">Klien = </h7>
                                    <h5 class="text-navy">{TimesheetModel.placement_id}</h5>
                                    
                                    <h7 class="text-navy">Jam Kerja = </h7>
                                    <h5 class="text-navy">{TimesheetModel.start} - {TimesheetModel.end}</h5>
                                    
                                    <h7 class="text-navy">Jam Overtime = </h7>
                                    <h5 class="text-navy">{TimesheetModel.start_ot} - {TimesheetModel.end_ot}</h5>
                                    
                                    
                                </div>
                            </div>
                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <button type="button" className="btn bg-orange" onClick={handleCancel}  >
                            Batal
                    </button>
                    </Modal.Footer>

                </Modal>
                <Modal show={openmodalcoba} style={{ opacity: 1, paddingTop: "10%", paddingLeft: "5%" }}>
                    <ModalHeader style={{ backgroundColor: "navy" }}>
                        <Modal.Title>
                            <div class="text-white"> Pilih ini ? </div>
                        </Modal.Title>
                    </ModalHeader>
                    {/* <Modal.Body>
                        <label>Stok : {barang.Qty} </label> <br/>
                        <label>Harga : {barang.Harga} </label> <br/>
                        <input type="text" id="qtybeli" onChange={onInputQty("qtybeli")}/>
                    </Modal.Body> */}
                    <Modal.Footer>

                        <div class="btn-group">
                            <button type="button" className="btn bg-orange" onClick={handleCancel}  >      Cancel      </button>
                            <button type="button" className="btn bg-navy" onClick={onSetCheck}       >      Ok !    </button>
                        </div>
                    </Modal.Footer>
                </Modal>
                <Modal show={openmodalcobaall} style={{ opacity: 1, paddingTop: "10%", paddingLeft: "5%" }}>
                    <ModalHeader style={{ backgroundColor: "navy" }}>
                        <Modal.Title>
                            <div class="text-white"> Pilih ini SEMUA ? </div>
                        </Modal.Title>
                    </ModalHeader>
                    {/* <Modal.Body>
                        <label>Stok : {barang.Qty} </label> <br/>
                        <label>Harga : {barang.Harga} </label> <br/>
                        <input type="text" id="qtybeli" onChange={onInputQty("qtybeli")}/>
                    </Modal.Body> */}
                    <Modal.Footer>

                        <div class="btn-group">
                            <button type="button" className="btn bg-orange" onClick={handleCancel}  >      Cancel      </button>
                            <button type="button" className="btn bg-navy" onClick={onSetCheckAll}       >      Ok !    </button>
                        </div>
                    </Modal.Footer>
                </Modal>

            </div>
        )
    }
}
export default FormDetail