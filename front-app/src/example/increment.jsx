import React from 'react';

class Increment extends React.Component{

    render(){
        const {funcIncrement}= this.props;
        return(
            <div>
                <button onClick={funcIncrement}>  Hitung </button>
            </div>
        )
    }
}

export default Increment