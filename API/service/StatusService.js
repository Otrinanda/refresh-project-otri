module.exports = exports = (server, pool) => {
    //ambil semua data
    server.get('/api/getstatus', (req, res) => {

        pool.query(`SELECT*FROM  public."Status" `, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                });
            }
        })
    });
}