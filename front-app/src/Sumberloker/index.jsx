import React from 'react';
import Table from 'react-bootstrap/Table';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import LokerService from '../service/LokerService';

//import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import FormLoker from './formloker';
import FormBiodata from './formbiodata';
import { config } from '../config/config';

class Loker extends React.Component {

    LokerModel = {
        id: 0,
        createdBy: 0,
        createdOn: "",
        modifiedBy: 0,
        modifiedOn: "",
        deleteBy: 0,
        deleteOn: "",
        biodataId: 0,
        vacancySource: "",
        candidateType: "",
        eventName: "",
        careerCenterName: "",
        referrerName: "",
        referrerPhone: "",
        referrerEmail: "",
        otherSource: "",
        lastIncome: "",
        applyDate: "",
        w: true,
        x: true,
        y: true,
        z: true,
        fullname: "",
        identity_no: "",
        sementara: "",
        vacancy_source: "",
        candidate_type: "",
        event_name: "",
        career_center_name: "",
        referrer_name: "",
        referrer_phone: "",
        referrer_email: "",
        other_source: "",





    }

    constructor() {
        super();
        this.state = {
            openmodalInput: false,
            openmodalBiodata: false,
            openmodalDetail: false,
            LokerModel: this.LokerModel,
            sumberOption: [],
            tipeOption: [],
            ListData: [],
            mode: "",
            errors: {}

        }
    }

    componentDidMount() {
        this.loadBiodata();
        this.setState({
            LokerModel: this.LokerModel,

        })
    }

    handleOpen = () => {
        this.getOptionSumber();
        this.getOptionTipe();
        this.setState({
            LokerModel: this.LokerModel,
            openmodalInput: true
        })
    }

    changeHandler = val => ({ target: { value } }) => {
        this.setState({
            LokerModel: {
                ...this.state.LokerModel,
                [val]: value,
                createdBy: localStorage.getItem(config.id)
            }
        })
    }

    getOptionSumber = async () => {
        const respons = await LokerService.getSumber();
        this.setState({
            sumberOption: respons.result
        })
    }

    getOptionTipe = async () => {
        const respons = await LokerService.getTipe();
        this.setState({
            tipeOption: respons.result
        })
    }

    handleCancel = async () => {
        this.setState({
            openmodalInput: false,
            errors: {}
        })
    }

    selectedSumber = (selectedOption) => {
        console.log(selectedOption.label)

        if (selectedOption.label === "Event") {
            this.setState({
                LokerModel: {

                    ...this.state.LokerModel,
                    vacancy_source: selectedOption.label,
                    w: false,
                    x: true,
                    y: true,
                    z: true
                }
            })

        }
        else if (selectedOption.label === "Carrer Center") {
            this.setState({
                LokerModel: {
                    ...this.state.LokerModel,
                    vacancy_source: selectedOption.label,
                    w: true,
                    x: false,
                    y: true,
                    z: true
                }
            })
        }


        else if (selectedOption.label === "Sumber Lainnya") {
            this.setState({
                LokerModel: {
                    ...this.state.LokerModel,
                    vacancy_source: selectedOption.label,
                    w: true,
                    x: true,
                    y: true,
                    z: false
                }
            })
        }
        else {
            this.setState({
                LokerModel: {
                    ...this.state.LokerModel,
                    vacancy_source: selectedOption.label,
                    w: true,
                    x: true,
                    y: false,
                    z: true
                }
            })
        }

    }

    selectedTipe = (selectedOption) => {
        this.setState({
            LokerModel: {
                ...this.state.LokerModel,
                candidate_type: selectedOption.label
            }
        })
    }

    loadBiodata = async () => {
        const respon = await LokerService.getBiodata();

        if (respon.success) {
            this.setState({
                ListData: respon.result
            })
        }
    }

    saveBiodata = async () => {
        const { LokerModel } = this.state;

        const respons = await LokerService.postBiodata(LokerModel);
        if (respons.success) {
            alert('Success : ' + respons.result)

        }
        else {
            alert('Error : ' + respons.result)
        }
        this.loadBiodata();
        this.setState({
            openmodalInput: false,
            // LokerModel: this.LokerModel,
            LokerModel: {
                ...this.LokerModel,

            }
        })
    }

    handleOpenBiodata = () => {
        this.setState({
            LokerModel: this.LokerModel,
            openmodalBiodata: true
        })
    }


    handleDetail = async (identity_no, id) => {

        const respon = await LokerService.getId(identity_no);
        if (respon.success) {
            if (respon.result === undefined) {
                //const respons = await LokerService.postBiodataId(identity_no);
                //if (respons.success) {
                alert('Successinput biodataId : ' + identity_no)
                console.log(identity_no)
                this.getOptionSumber();
                this.getOptionTipe();
                this.setState({
                    openmodalInput: true,
                    LokerModel: {
                        ...this.LokerModel,
                        identity_no: identity_no
                    },
                    mode: 'create'
                })
                //}
                //else {
                //  alert('ErrorinputbiodataId : ' + respons.result)
                //}
                alert('CreateMode')

            }
            else {
                const respons = await LokerService.getdetailbyid(identity_no);
                if (respons.success) {
                    alert('Success get detail by Id')
                    console.log(identity_no)
                    this.getOptionSumber();
                    this.getOptionTipe();
                    this.setState({
                        openmodalInput: true,
                        LokerModel: respons.result,
                        LokerModel: {
                            ...respons.result,
                            w: true,
                            x: true,
                            y: true,
                            z: true
                        },
                        mode: 'update'
                    })
                }
                else {
                    alert('Error getdetailbyId')
                }
                alert('UpdateMode')


            }
        }
    }

    handleValidation() {
        let fields = this.state.LokerModel;
        let errors = {};
        let formIsValid = true;

        if (fields["vacancy_source"] === "Event") {
            //eventName
            if (!fields["event_name"]) {
                formIsValid = false;
                errors["event_name"] = "event Name Cannot be empty";
            }
        }
        else if (fields["vacancy_source"] === "Carrer Center") {
            //careerCenterName
            if (!fields["career_center_name"]) {
                formIsValid = false;
                errors["career_center_name"] = "career Center NameCannot be empty";
            }
        }
        else if (fields["vacancy_source"] === "Sumber Lainnya") {
            //otherSource
            if (!fields["other_source"]) {
                formIsValid = false;
                errors["other_source"] = "other Source Cannot be empty";
            }
        }
        else if (fields["vacancy_source"] === "Referensi") {
            //referrerName
            if (!fields["referrer_name"]) {
                formIsValid = false;
                errors["referrer_name"] = "referrer Name Cannot be empty";
            }

            // //referrerPhone
            if (!fields["referrer_phone"]) {
                formIsValid = false;
                errors["referrer_phone"] = "referrer Phone Cannot be empty";
            }

            // //referrerEmail
            if (!fields["referrer_email"]) {
                formIsValid = false;
                errors["referrer_email"] = "referrer Email Cannot be empty";
            }
        }
        else {
            errors["vacancy_source"] = "vacancy Source Cannot be empty";
            formIsValid = false;
        }

        // //Tanggal Lamaran
        if (!fields["candidate_type"]) {
            formIsValid = false;
            errors["candidate_type"] = "candidate type  Cannot be empty";
        }

        // // //Gaji Terakhir
        // if (!fields["last_income"]) {
        //     formIsValid = false;
        //     errors["last_income"] = "income Cannot be empty";
        // }

        // //Tanggal Lamaran
        if (!fields["apply_date"]) {
            formIsValid = false;
            errors["apply_date"] = "apply date Cannot be empty";
        }

        this.setState({
            errors: errors
        });
        //console.log(errors)
        return formIsValid;
    }

    saveLoker = async () => {
        const { LokerModel, mode } = this.state;
        console.log(LokerModel)
        console.log(mode)
        if (this.handleValidation()) {
            if (mode === "update") {
                const respons = await LokerService.updateLoker(LokerModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadBiodata();

                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodalInput: false
                })
            }
            else {
                const respons = await LokerService.createLoker(LokerModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadBiodata();
                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({

                    openmodalInput: false
                })
            }
        }
        else {
            alert("Form has errors.")
        }
    }


    render() {
        const {
            openmodalInput,
            LokerModel,
            sumberOption,
            tipeOption,
            ListData,
            openmodalBiodata,
            errors,
            mode,
            saveBiodata,
            changeHandler,


        } = this.state;
        return (
            <div>
                <br />
                <br />

                {/* <FormBiodata
                    handleOpenBiodata={this.handleOpenBiodata}
                    changeHandler={this.changeHandler}
                    openmodalBiodata={openmodalBiodata}
                    LokerModel={LokerModel}
                    saveBiodata={this.saveBiodata}
                /> */}
                <FormLoker
                    handleOpen={this.handleOpen}
                    changeHandler={this.changeHandler}
                    openmodalInput={openmodalInput}
                    handleCancel={this.handleCancel}
                    LokerModel={LokerModel}
                    sumberOption={sumberOption}
                    selectedSumber={this.selectedSumber}
                    tipeOption={tipeOption}
                    selectedTipe={this.selectedTipe}
                    onSave={this.onSave}
                    saveLoker={this.saveLoker}
                    errors={errors}
                    mode={mode}

                />

                <div className='box box-primary box-solid'>
                    <div class="box-header with-border">
                        <h5 class="box-title">Tambah Pelamar</h5>
                    </div>
                    <div class="box-body">
                        <div className="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label className="form-label"> Nama Anda </label>
                                    <br />
                                    <input type="text" id='fullname'
                                        className="form-control" value={LokerModel.fullname}
                                        onChange={this.changeHandler("fullname")} />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label className="form-label"> ID Biodata Anda </label>
                                    <br />
                                    <input type="text" id='identity_no'
                                        className="form-control" value={LokerModel.identity_no}
                                        onChange={this.changeHandler("identity_no")} />
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <Button variant='dark' class="fa fa-search"
                                    onClick={this.saveBiodata}>
                                    <i class="fa fa-fw fa-user-plus"></i>
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='box box-primary box-solid'>
                    <div class="box-header with-border">
                        <h5 class="box-title">Data Pelamar</h5>
                    </div>
                    <div class="box-body">
                        <Table striped bordered hover>

                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Biodata ID</th>


                                </tr>
                            </thead>
                            <tbody>
                                {ListData.map(data => {
                                    return (
                                        <tr>
                                            <td>{data.fullname}</td>
                                            <td>{data.identity_no}</td>
                                            <td>
                                                <Button variant='dark' class="fa fa-search"
                                                    onClick={() => this.handleDetail(data.identity_no)}>
                                                    <i class="fa fa-search"></i>
                                                </Button>
                                            </td>

                                        </tr>
                                    )
                                })}
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        )
    }
}

export default Loker;