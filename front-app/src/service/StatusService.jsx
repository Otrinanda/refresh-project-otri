import axios from 'axios';
import {config} from '../config/config';
 const StatusService ={
    getStatus :()=>{
        const result = axios.get(config.apiUrl+'/getstatus')
        .then(response =>{
            return {
                success:response.data.success,
                result:response.data.result
            }
        })
        .catch(error =>{
            return{
                success:false,
                result:error
            }
        });
        return result
    }
}
    export default StatusService;